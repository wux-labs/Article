@[TOC](【力扣算法题-Python】第13题、罗马数字转整数)

# 题目

题目：罗马数字转整数。

难度：简单。

罗马数字包含以下七种字符: `I`， `V`， `X`， `L`，`C`，`D` 和 `M`。

```
字符          数值
I             1
V             5
X             10
L             50
C             100
D             500
M             1000
```

例如， 罗马数字 `2` 写做 `II` ，即为两个并列的 1 。`12` 写做 `XII` ，即为 `X` + `II` 。 `27` 写做 `XXVII`, 即为 `XX` + `V` + `II` 。

通常情况下，罗马数字中小的数字在大的数字的右边。但也存在特例，例如 4 不写做 `IIII`，而是 `IV`。数字 1 在数字 5 的左边，所表示的数等于大数 5 减小数 1 得到的数值 4 。同样地，数字 9 表示为 `IX`。这个特殊的规则只适用于以下六种情况：

- `I` 可以放在 `V` (5) 和 `X` (10) 的左边，来表示 4 和 9。
- `X` 可以放在 `L` (50) 和 `C` (100) 的左边，来表示 40 和 90。
- `C` 可以放在 `D` (500) 和 `M` (1000) 的左边，来表示 400 和 900。

给定一个罗马数字，将其转换成整数。



**示例 1:**

```
输入: s = "III"
输出: 3
```

**示例 2:**

```
输入: s = "IV"
输出: 4
```

**示例 3:**

```
输入: s = "IX"
输出: 9
```

**示例 4:**

```
输入: s = "LVIII"
输出: 58
解释: L = 50, V= 5, III = 3.
```

**示例 5:**

```
输入: s = "MCMXCIV"
输出: 1994
解释: M = 1000, CM = 900, XC = 90, IV = 4.
```



**提示：**

- `1 <= s.length <= 15`
- `s` 仅含字符 `('I', 'V', 'X', 'L', 'C', 'D', 'M')`
- 题目数据保证 `s` 是一个有效的罗马数字，且表示整数在范围 `[1, 3999]` 内
- 题目所给测试用例皆符合罗马数字书写规则，不会出现跨位等情况。
- IL 和 IM 这样的例子并不符合题目要求，49 应该写作 XLIX，999 应该写作 CMXCIX 。
- 关于罗马数字的详尽书写规则，可以参考 [罗马数字 - Mathematics ](https://b2b.partcommunity.com/community/knowledge/zh_CN/detail/10753/罗马数字#knowledge_article)。

Related Topics

哈希表

数学

字符串



👍 2228

👎 0

# 思路

从罗马数字的构成看，只需要按照从左到右的顺序按规则将罗马字母转换成对应代表的数字，然后相加即为最终的整数，几乎不存在进位的问题。

# 解题

## 暴力破解法

这种方式中，直接统计每种字符出现的次数，然后分别乘以每种字符代表的数字后相加，如果出现特殊数字的，需要从最终结果中减去。

比如：MCMXCIV

首先按字符直接统计：

```
count(M) * 1000 + count(C) * 100 + count(X) * 10 + count(I) * 1 + count(V) * 5
```

由于CM是特殊数字，不能直接按字符加，所以需要从上面减去多加的部分：

```
(count(M)-count(CM)) * 1000 + (count(C)-count(CM)) * 100 + count(X) * 10 + count(I) * 1 + count(V) * 5
```

同时，CM本身代表的数字需要加上，CM是900，由于900 = 1000 - 100，所以CM=M-C，再在上面的基础上加上CM：

```
(count(M)-count(CM)) * 1000 + (count(C)-count(CM)) * 100 + count(X) * 10 + count(I) * 1 + count(V) * 5 + count(CM) * 900
=
(count(M)-count(CM)) * 1000 + (count(C)-count(CM)) * 100 + count(X) * 10 + count(I) * 1 + count(V) * 5 + count(CM) * (1000 - 100)
=
(count(M)-count(CM)) * 1000 + (count(C)-count(CM)) * 100 + count(X) * 10 + count(I) * 1 + count(V) * 5 + count(CM) * 1000 - count(CM) * 100
=
count(M) * 1000 + (count(C) - 2 * count(CM)) * 100 + count(X) * 10 + count(I) * 1 + count(V) * 5
```

对于XC、IV的处理与CM的一致，所以结果是：

```
count(M) * 1000 + (count(C) - 2 * count(CM)) * 100 + (count(X) - 2 * count(XC)) * 10 + (count(I) - 2 * count(IV)) * 1 + count(V) * 5
```

最终等于：

```
2 * 1000 + (2 - 2 * 1) * 100 + (1 - 2 * 1) * 10 + (1 - 2 * 1) * 1 + 1 * 5
=
2000 + 0 + (-10) + (-1) + 5
= 
1994
```

因此，只需要对字符串中的字符做计数统计，再按照规则进行相应的计算即可。

代码如下：

```python
class Solution:
    def romanToInt(self, s: str) -> int:
        i = s.count("I")
        v = s.count("V")
        x = s.count("X")
        l = s.count("L")
        c = s.count("C")
        d = s.count("D")
        m = s.count("M")
        cm = s.count("CM")
        cd = s.count("CD")
        iv = s.count("IV")
        ix = s.count("IX")
        xl = s.count("XL")
        xc = s.count("XC")
        return m * 1000 + 500 * d + (c - 2 * (cm + cd)) * 100 + 50 * l + (x - 2 * (xl + xc)) * 10 + v * 5 + (i - 2 * (iv + ix)) * 1
```

提交执行。

```
> 2023/02/07 23:35:15	
Success:
	Runtime:48 ms, faster than 75.12% of Python3 online submissions.
	Memory Usage:15 MB, less than 69.46% of Python3 online submissions.
```

![image-20230207233658858](images/image-20230207233658858.png)

## 常规解题法

按照题目描述发现：

* 特殊数字都是两位字符组成，并且左边的字符代表的数字比右边字符代表的数字小
* 特殊数字的值，都可以用右边字符代表的数值减去左边字符代表的数值

那么，就可以按照字符串按位截取的方式进行计算，遇到右边大于左边的，用右边的值减去左边的值，否则就逐位相加。

代码如下：

```python
class Solution:
    def romanToInt(self, s: str) -> int:
        dict = {'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100, 'D': 500, 'M': 1000}
        if len(s) == 1:
            return int(dict[s])
        result = 0
        for i in range(len(s)):
            if i < len(s) - 1 and dict[s[i]] < dict[s[i + 1]]:
                result = result - dict[s[i]]
            else:
                result = result + dict[s[i]]
        return result
```

提交执行。

```
> 2023/02/07 23:44:21	
Success:
	Runtime:48 ms, faster than 75.12% of Python3 online submissions.
	Memory Usage:14.9 MB, less than 89.58% of Python3 online submissions.
```

![image-20230207234519526](images/image-20230207234519526.png)

## 自定义值法

根据题目描述，罗马数字是由字母代表数字的，并且只有有限的几个字母和几个特殊的组合。那么，我们可以利用自定义的字母代表特殊数字的方式来做，需要自定义的值有：

```python
dict = {'O': 4, 'P': 9, 'Q':40, 'R': 90, 'S': 400, 'T': 900}
```

代码如下：

```python
class Solution:
    def romanToInt(self, s: str) -> int:
        dict = {'I': 1, 'O': 4, 'V': 5, 'P': 9, 'X': 10, 'Q':40, 'L': 50, 'R': 90, 'C': 100, 'S': 400, 'D': 500, 'T': 900, 'M': 1000}
        s=s.replace("IV","O").replace("IX","P").replace("XL","Q").replace("XC", "R").replace("CD","S").replace("CM","T")
        sum = 0
        for x in s:
            sum += dict[x]
        return sum
```

提交执行。

```
> 2023/02/07 23:51:16	
Success:
	Runtime:44 ms, faster than 89.34% of Python3 online submissions.
	Memory Usage:15 MB, less than 47.67% of Python3 online submissions.
```

![image-20230207235321371](images/image-20230207235321371.png)

# 最后

该题的解法还是比较多的，对于Python3，运行时间上在 32 ms可以超过99.8%的用户，内存空间上14.8 MB可以超过97.64%的用户。
