# Placement Driver

## PD的架构

![image-20230203233454018](images/image-20230203233454018.png)

PD集群也是主从结构的，有一个leader，另外的节点是follower。集成了ETCD，其Raft协议保证了数据的一致性。

PD中存储了数据的元数据信息，同时提供Region的调度功能。

PD提供全局时钟TSO的功能。

Store：集群中的存储实例，对应TiKV。

Region：数据的存储单元，负责维护存储的数据。包含多个副本，每个副本叫一个Peer。读写操作在主节点（leader节点）进行，相关信息通过Raft协议同步到follower节点。leader和follower之间组成一个group，就是raft group。

PD的主要功能：

* 整个集群的TiKV的元数据存储
* 分配全局ID和事务ID
* 生成全局时间戳TSO
* 收集集群中TiKV的信息，进行调度
* 提供label，支持高可用
* 提供TiDB Dashboard

## 路由功能

![image-20230203234359393](images/image-20230203234359393.png)

TiDB Server接收用户的SQL请求，执行器Executor通过TiKV Client与TiKV进行交互，TiKV Client需要先从PD获取到Region的元数据信息。

TiKV Client从PD获取Region的元数据信息，需要与PD进行交互，这会涉及到网络开销、数据传输，为了提高效率，减少数据的网络传输，TiKV Client本地可以将Region的元数据信息缓存起来，供下次使用，这个缓存是Region Cache。

back off：TiKV Client从PD拿到leader信息后到Region进行读取，如果此时原有leader失效，变成follower了，则Region会反馈信息给TiKV Client，让TiKV Client重新去新的leader节点读取数据，这个过程就是back off。back off越多，读取数据的延迟就越高，效率越低。

## TSO分配

### 概念

TSO = physical time logical time

TSO是一个int64的整数；physical time是物理时钟，精确到毫秒；logical time是逻辑时钟，将1号码的物理时钟划分成262144个逻辑时钟。

### 过程

![image-20230203235538073](images/image-20230203235538073.png)

PD Client包含批处理功能，客户会话请求并发低的时候，PD Client一次可能只取一个TSO；客户会话请求并发高的时候，PD Client可以一次获取一批TSO，减少与PD的交互，提高本地响应效率。

### 性能问题

![image-20230204001235043](images/image-20230204001235043.png)

PD节点可能会宕机，PD需要将当前已分配的TSO记录存储到磁盘，防止宕机的时候数据丢失。写磁盘是有磁盘IO的，每次分配一个TSO、写一次磁盘，性能会比较低。PD采用每次分配3秒钟的TSO，在内存缓存中记录了3秒钟的TSO缓存供高并发的TiDB Server排队取用，存储在磁盘的是时间区间，3秒钟写一次磁盘，以提高效率。写磁盘的数据会通过Raft协议同步到其他PD节点。

### 高可用问题

PD每次缓存3秒的TSO，磁盘3秒钟写一次，磁盘数据通过Raft协议同步到其他节点。

当leader节点宕机，leader节点的缓存会丢失，但磁盘的数据已同步到其他PD节点。其他PD节点因无主而会重新选举leader节点，新leader节点产生后，仅会知道磁盘中存储的数据，原leader中的缓存数据无法恢复。为了保证TSO的单调递增特性，新leader不会再从磁盘中存储的时间窗口内分配TSO，而是新启动一个3秒钟的时间窗口分配TSO。这会导致TSO不严格连续，但能保证TSO单调递增。

## 调度

### 信息收集

PD通过心跳收集TiKV的信息。

主要包括：Store的心跳和Region的心跳。

![image-20230204002235296](images/image-20230204002235296.png)



### 生成调度

生成调度（operator）的主要关注的信息包括：

负载均衡、热点Region、集群拓扑、缩容、故障恢复、Region合并等。

### 执行调度

PD生成好operator后，会将operator发送到相关节点进行执行。

![image-20230204002623601](images/image-20230204002623601.png)

## Label与高可用

![image-20230204002947612](images/image-20230204002947612.png)

DC：数据中心

Rack：机柜

对于Region1，由于有两个Region在同一个机柜，所以存在一个机柜坏了后Region1不可用的情况。

对于Region2，由于有两个Region在同一个数据中心，所以存在一个数据中心出问题后Region2不可用的情况。

对于Region3，分布在不同的数据中心、不同的机柜，所以就算一个数据中心出问题，Region3仍然可用。

label，PD的标签功能，主要用于标记一个Region如何分布，PD根据label选择将Region分配到哪里。

### 配置

![image-20230204003540150](images/image-20230204003540150.png)

TiKV实例会记录自己的信息：server.labels

PD实例上配置label的规则，包括：副本数、隔离级别等信息

PD与TiKV配合使用即可实现label的分配和高可用。

# 知识点回顾

1. 下列关于 PD（Placement Driver）架构和功能正确的是？

 A. 访问 PD 集群中的任何一个节点都可以获得 TSO 

 B. TiKV 会周期性地向 PD 上报状态 

 C. PD 会周期性地查询 TiKV 的状态，不需要 TiKV 上报，目的是为了高效 

 D. PD 的调度功能只能平衡 region 的分布，无法对 leader 进行调度 

解析：PD主要是用来存储元数据、分配TSO、执行调度，PD是主从架构的，需要与leader节点进行交互。



2. 关于 label ，下列说法不正确的是？

 A. label 的本质是个调度系统，可以人为控制 region 副本的存放位置 

 B. label 需要在 PD 和 TiKV 上进行配置 

 C. isolation-level 要和数据中心（DC）对应，这样可以获得最大的可用性 

 D. 如果某个 region 的所有副本不可用，有可能造成整个 TiDB 数据库不可用 

解析：label主要是用于实现高可用的，防止机柜、数据中心不可用导致Region不可用，label可人为配置，需要PD和TiKV配合使用
