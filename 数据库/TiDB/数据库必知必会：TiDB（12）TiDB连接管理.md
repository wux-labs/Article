# TiDB连接管理

## TiDB的连接特性

TiDB Server主要负责接收用户的会话请求，接收SQL并负责SQL语句的解析、编译，生成SQL的执行计划。TiDB Server并不存储数据，所以TiDB的连接是无状态的，用户连接的TiDB Server节点出现问题后，用户会话将会丢失，但其他节点的会话不受影响，丢失会话的用户需要重新连接到其他TiDB Server节点，重新执行SQL。

TiDB完全兼容MySQL 5.7协议，支持MySQL 5.7规范支持的常用SQL语句，但是不支持以下语句：

* 存储过程
* 自定义函数
* 触发器
* 外键
* CTAS语法（create table ... as select ... from ...）

## 连接TiDB

### MySQL命令行客户端

TiDB完全兼容MySQL 5.7，所以TiDB可以使用MySQL客户端进行连接。

首先，在服务器上安装MySQL客户端，命令为：

```
sudo apt-get install mysql-client
```

安装过程：

```
wux_labs@wux-labs-vm:~$ sudo apt-get install mysql-client
Reading package lists... Done
Building dependency tree       
Reading state information... Done
The following additional packages will be installed:
  mysql-client-8.0 mysql-client-core-8.0 mysql-common
The following NEW packages will be installed:
  mysql-client mysql-client-8.0 mysql-client-core-8.0 mysql-common
0 upgraded, 4 newly installed, 0 to remove and 6 not upgraded.
Need to get 5196 kB of archives.
After this operation, 75.6 MB of additional disk space will be used.
Do you want to continue? [Y/n] y
Get:1 http://azure.archive.ubuntu.com/ubuntu focal-updates/main amd64 mysql-client-core-8.0 amd64 8.0.32-0ubuntu0.20.04.2 [5157 kB]
Get:2 http://azure.archive.ubuntu.com/ubuntu focal/main amd64 mysql-common all 5.8+1.0.5ubuntu2 [7496 B]
Get:3 http://azure.archive.ubuntu.com/ubuntu focal-updates/main amd64 mysql-client-8.0 amd64 8.0.32-0ubuntu0.20.04.2 [22.0 kB]
Get:4 http://azure.archive.ubuntu.com/ubuntu focal-updates/main amd64 mysql-client all 8.0.32-0ubuntu0.20.04.2 [9364 B]
Fetched 5196 kB in 1s (10.1 MB/s)      
Selecting previously unselected package mysql-client-core-8.0.
(Reading database ... 84042 files and directories currently installed.)
Preparing to unpack .../mysql-client-core-8.0_8.0.32-0ubuntu0.20.04.2_amd64.deb ...
Unpacking mysql-client-core-8.0 (8.0.32-0ubuntu0.20.04.2) ...
Selecting previously unselected package mysql-common.
Preparing to unpack .../mysql-common_5.8+1.0.5ubuntu2_all.deb ...
Unpacking mysql-common (5.8+1.0.5ubuntu2) ...
Selecting previously unselected package mysql-client-8.0.
Preparing to unpack .../mysql-client-8.0_8.0.32-0ubuntu0.20.04.2_amd64.deb ...
Unpacking mysql-client-8.0 (8.0.32-0ubuntu0.20.04.2) ...
Selecting previously unselected package mysql-client.
Preparing to unpack .../mysql-client_8.0.32-0ubuntu0.20.04.2_all.deb ...
Unpacking mysql-client (8.0.32-0ubuntu0.20.04.2) ...
Setting up mysql-common (5.8+1.0.5ubuntu2) ...
update-alternatives: using /etc/mysql/my.cnf.fallback to provide /etc/mysql/my.cnf (my.cnf) in auto mode
Setting up mysql-client-core-8.0 (8.0.32-0ubuntu0.20.04.2) ...
Setting up mysql-client-8.0 (8.0.32-0ubuntu0.20.04.2) ...
Setting up mysql-client (8.0.32-0ubuntu0.20.04.2) ...
Processing triggers for man-db (2.9.1-1) ...
wux_labs@wux-labs-vm:~$ 
```

![image-20230218215753201](images/image-20230218215753201.png)

安装完成后，确保mysql命令可以使用。

![image-20230218215847032](images/image-20230218215847032.png)

MySQL命令行客户端连接数据库的语法为：

```
mysql -h ${host} -P ${port} -u ${username} -p ${password}
```

对于TiDB：

* ${host}是TiDB Server的主机地址
* ${port}是TiDB Server的服务端口，默认配置是 4000

给定具体参数值，连接TiDB Server：

```
mysql -hwux-labs-vm -P4000 -uroot -p'@2XKr^+9&nNZ3U07q6'
```

连接过程：

```
wux_labs@wux-labs-vm:~$ mysql -hwux-labs-vm -P4000 -uroot -p'@2XKr^+9&nNZ3U07q6'
mysql: [Warning] Using a password on the command line interface can be insecure.
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 459
Server version: 5.7.25-TiDB-v6.1.0 TiDB Server (Apache License 2.0) Community Edition, MySQL 5.7 compatible

Copyright (c) 2000, 2023, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql>
```

![image-20230218220414486](images/image-20230218220414486.png)

这样就连接上了TiDB集群了。接下来可以执行一些命令。

* 查看当前已有的库

```
show databases;
```

![image-20230218220638736](images/image-20230218220638736.png)

当前默认创建了一个数据库test。

* 切换数据库

```
use test;
```

![image-20230219152048439](images/image-20230219152048439.png)

* 查看表

```
show tables test;
```

当前没有表，所以是空的。

![image-20230219152135338](images/image-20230219152135338.png)

* 创建表

```sql
create table dept(
  id int,
  name varchar(100)
);
```

![image-20230219152231945](images/image-20230219152231945.png)

* 插入数据

```sql
insert into dept values(1, 'Labs');
```

![image-20230219152414831](images/image-20230219152414831.png)

* 查询数据

```
select * from dept;
```

![image-20230219152431912](images/image-20230219152431912.png)

* 修改用户密码

```sql
set password = password('tidb');
```

![image-20230219152518024](images/image-20230219152518024.png)

* 查询已有用户

```sql
select user,host,authentication_string from mysql.user;
```

![image-20230219152707643](images/image-20230219152707643.png)

### 图形界面客户端连接

除了使用MySQL命令行工具连接TiDB，还可以使用图形界面客户端工具。

本文使用DataGrip作为图形界面客户端做演示，其他还有很多图形界面客户端也是支持的。

通过添加数据源菜单，选择MySQL数据源。

![image-20230219153057010](images/image-20230219153057010.png)

在数据源配置界面输入TiDB的连接信息，注意端口是4000。

![image-20230219153405966](images/image-20230219153405966.png)

点击Test Connection连接测试连接，确保连接成功。

![image-20230219153503614](images/image-20230219153503614.png)

连接成功后，可以执行相关的命令、语句。

* 查看当前已有的数据库

```sql
show databases;
```

![image-20230219153724095](images/image-20230219153724095.png)

* 切换数据库

```sql
use test;
```

执行完成后，当前数据库将会切换成test。

![image-20230219154012158](images/image-20230219154012158.png)

* 查询数据

```sql
select * from dept;
```

执行完成后，将以表格形式显示查询结果。

![image-20230219154138060](images/image-20230219154138060.png)

### 其他连接方式

TiDB支持的连接器和API：

* MySQL Connector/Net
* MySQL Connector/ODBC
* MySQL Connector/Python
* MySQL C API
* MySQL PHP API
* MySQL Python API
* MySQL GO API

# 写在后面

TiDB完全兼容MySQL 5.7协议，因此支持的客户端连接工具比较多，选择一个自己喜欢的客户端用起来吧。
