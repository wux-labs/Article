# TiFlash

## 架构

![image-20230205151602501](images/image-20230205151602501.png)

TiFlash对TiKV的影响极低，只是存储一个TiKV的列存版本，通过Raft Learner实现Raft日志的异步复制，TiFlash不支持直接写入，但可以支持读取。

TiFlash主要是做OLAP，QPS<50。

## 主要功能

### 异步复制

![image-20230205152105108](images/image-20230205152105108.png)

TiFlash以Learner的角色接入TiKV，不参与Raft投票、Raft选举，TiFlash是异步复制的，TiKV不需要关心TiFlash的写入是否成功。

### 一致性读取

TiFlash也支持多个时间戳的版本，读取TiFlash的时候会与TiKV的leader进行交互，进行进度校验，确保读取数据的一致性。

![image-20230205152245005](images/image-20230205152245005.png)

T0时刻客户端写入数据，假设，两条数据分别写到两个TiKV节点，日志都会写到raft log。此时，TiFlash中两个region的数据都还没同步过来，因为复制是异步的。

![image-20230205152628597](images/image-20230205152628597.png)

T1时刻，有客户端请求TiFlash，需要读取最新的数据。

![image-20230205153338439](images/image-20230205153338439.png)

T2时刻客户端又写了一笔数据。

![image-20230205153415003](images/image-20230205153415003.png)

T3时刻，TiFlash向Region确认当前Raft log的位置，记录下当前最新的Raft log的位置，然后自己一直等到记录的最新位置的Raft log复制到TiFlash。

![image-20230205153655639](images/image-20230205153655639.png)

T4时刻，TiFlash确保自己的日志已经成功复制到了之前记录的TiKV Region的Raft log位置，此时可认为之前的日志都被Apply了。

![image-20230205153858336](images/image-20230205153858336.png)

T5时刻，绿色region的最新日志已经复制到了TiFlash，但是数据已经在T2时刻被修改过一次，所以TiFlash上有数据的两个版本。该读哪个版本？因为读取请求是在T1时刻发起的，根据快照隔离，T1时刻是读取不到T2的写入的，所以会返回T0时刻的版本数据。

### 引擎智能选择

根据SQL，引擎自动判断是分析型SQL还是交易型SQL，从而确定查TiKV还是TiFlash。

![image-20230205154127204](images/image-20230205154127204.png)

案例中，product表按batch_id进行扫描，是一个索引扫描，适合走行存数据读取；sales表中需要对price进行avg的聚合操作，需要做全表扫描，适合从列存读取数据。

### 计算加速

# 知识点回顾

1. 下面属于 TiFlash 核心特性的是？（请选择 3 项）

A. 采用行存 + 列存的混合存储方式

B. region 支持 raft 投票和选举

C. TiFlash 采用异步复制来保证和 TiKV 一致

D. 在 TiKV 上写入数据成功后，在 TiFlash 上可以一致性读取

E. CBO 基于成本选择在 TiFlash 或者 TiKV 上执行 SQL

解析：TiFlash的存储方式是列存，采用learner方式异步复制Raft日志，确保数据的一致性，不参与投票及选举



2. 关于 TiFlash 的使用，描述不正确的是？

 A. TiFlash 不善于处理高并发，QPS 一般不应过高 

 B. SQL 语句执行中，要不然数据完全从 TiKV 中读取，要不然完全从 TiFlash 中读取 

 C. MPP 中表连接前的过滤和交换完全是在 TiFlash 节点上完成的 

 D. 在读取 TiFlash 中数据的时候，我们需要通过 TiKV 中的数据确认一致性 

解析：本题选不正确的，TiFlash适合OLAP，并发低，QPS低，根据SQL的解析结果，不同的表可采取不同的读取策略
