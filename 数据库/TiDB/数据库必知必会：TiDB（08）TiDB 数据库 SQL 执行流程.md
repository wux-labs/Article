# 数据库 SQL 执行流程

在TiDB中三个重要组件：

PD：分配TSO、存储数据的元数据信息。

TiDB Server：接收用户的SQL请求，从PD获取TSO和元数据信息，对SQL进行解析、优化、生成执行计划到TiKV中读取数据。

TiKV：数据持久化。

## DML语句读流程

### 概述

![image-20230204214350701](images/image-20230204214350701.png)

TiDB Server中的Protocol Layer首先接收用户的SQL请求，TiDB Server会到PD获取TSO，同时经过解析模块Parse对SQL进行词法分析和语法分析，再由Compile模块进行编译，最终进行Execute。

### SQL的Parse与Compile

![image-20230204215904907](images/image-20230204215904907.png)

Protocol Layer接收到SQL请求后，会由PD Client与PD进行交互，获取TSO。

Parse模块会对SQL进行词法分析、语法分析，生成抽象语法树AST。

Compile分成几个阶段：

* 预处理preprocess，检测SQL的合法性、绑定信息等，判断SQL是否是点查SQL（仅查询一条数据，比如按Key查询），如果是点查语句则不需要执行optimize优化，直接执行即可。
* 优化optimize，逻辑优化，根据关系代数、等价交换等对SQL进行逻辑变换，比如外连接尝试转内连接等；物理优化，基于逻辑优化的结果和统计信息，选择最优的算子。

### SQL的Execute

![image-20230204220606319](images/image-20230204220606319.png)

Compile的产物是执行计划，有了执行计划之后就由Executor进行执行。

Executor先获取information schema，information schema可以预先从TiKV加载到TiDB Server。然后Executor需要从PD获取Region的元数据信息，为了减少TiDB Server与PD交互带来的网络开销、延迟，TiKV Client的region Cache可以缓存Region的元数据信息，后续就可以直接使用。

![image-20230204221558022](images/image-20230204221558022.png)

Executor执行数据读取有两种类型，点查SQL，直接读取KV；复杂SQL，需要通过DistSQL将复杂SQL转换成对单表的简单查询SQL。

TiKV接收到读取请求后，会创建一个数据快照snapshot，所有查询SQL都会进入UnifyRead Pool线程池，然后从RocksDB KV读取数据。

![image-20230204222600102](images/image-20230204222600102.png)

当数据读取完成后，数据通过TiKV Client返回给TiDB Server。

由于TiDB实现了算子下推，对于聚合操作，TiKV完成cop task，TiDB Server完成root task，也就是在TiDB Server中还需要对下推算子的聚合结果进行汇总。

## DML语句写流程

### 概述

![image-20230204215037040](images/image-20230204215037040.png)

写流程，会先经过一次读流程，将数据读取到缓存MemBuffer中，然后再进行数据修改，最后进行两阶段提交进行数据写入。

### 执行

![image-20230204222934633](images/image-20230204222934633.png)

Transaction执行两阶段提交。

Transaction按行读取memBuffer的数据进行数据写入，事务包含两个TSO，一个是事务开始TSO，一个是事务提交TSO。

![image-20230204223227300](images/image-20230204223227300.png)

写请求发送给TiKV的Scheduler，Scheduler是接收并发处理的，所需需要负责协调冲突写入（两个会话并行写相同的Key），冲突写入采用分配latch，谁获得latch谁执行写操作的方式解决冲突。无冲突的写入，交Raftstore，完成Raft日志写入过程，涉及本地append、replicate、commited、apply等过程。

## DDL语句流程

### 概要

![image-20230204215450404](images/image-20230204215450404.png)

TiDB Server接收到用户的DDL请求，start job模块将操作写入队列，由TiDB Server的Owner角色的workers模块负责执行队列中的DDL语句。

workers从job queue队列中获取待执行的DDL语句，执行，执行完成后将其放到history queue队列中。

### 执行

TiDB支持Online DDL，也就是DDL不锁表，不阻塞读、写。

![image-20230204223747117](images/image-20230204223747117.png)

同一时刻，只有一个TiDB Server的角色是Owner角色的，只有Owner角色的TiDB Server的workers才可以执行DDL。schema load负责将最新的表结构信息加载到TiDB Server。

Compile生成的执行计划，交给start job，start job先检查本机节点的角色是否是Owner，如果是，则可直接由本机workers直接执行，如果不是，则start job将DDL操作封装成job加入到job queue队列中。如果DDL操作是对索引的操作，则job会加入到add index queue。

![image-20230204224520664](images/image-20230204224520664.png)

Owner的workers会定时扫描job queue，发现queue中有job就获取执行，执行完成后将job加入到history queue中。

Owner角色由PD协调，在TiDB Server之间是轮询切换，所以总体来说，每个TiDB Server都有机会成为Owner。

# 知识点回顾

1. 下列关于 DML 语句读写说法正确的是？（ 选 2 项 ）

A. Region Cache 的主要作用是缓存热数据，减少访问 TiKV 的次数

B. 二阶段提交在获取事务开始的 TSO 和提交的 TSO 时，都是由 TiDB Server 完成的

C. schedule 模块采用 latch 来控制当前正在写的数据不被读取

D. 在写操作中，锁信息也会被写入到 RocksDB KV 中

解析：Region Cache主要缓存Region的元数据信息，TSO都是由PD分配、由TiDB Server从PD获取



2. 关于 DDL 语句的执行流程，下列说法正确的是？

A. DDL 语句不可以在 TiDB 中并行执行

B. 同一时刻，不可以有多条 DDL 语句在等待执行

C. 同一时刻，只有一个 TiDB Server 可以执行 DDL 语句

D. 等待执行的 DDL 语句被持久化在 TiDB Server 的存储中

解析：DDL语句封装成job持久化到TiVK的queue中，由Owner角色的workers从queue中获取执行，queue中可以包含多条等待执行的DDL。
