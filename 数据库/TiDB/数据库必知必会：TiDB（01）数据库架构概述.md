# TiDB体系架构

TiDB兼容MySQL 5.7协议，支持水平扩容或者缩容的金融级高可用的云原生分布式数据库。

TiDB的体系架构为：

![image-20230128224256490](images/image-20230128224256490.png)

**TiDB Server**，接收用户会话，解析、编译、优化用户提交的SQL语句，生成执行计划。TiDB Server是无状态的，不存储数据，为了提高并发处理能力，TiDB Server支持水平扩展。

**TiKV**，用于存储数据，数据按行存储，按Region组织数据，一个Region的大小在96MB~144MB之间，一个Region默认采用Raft协议创建3个副本，提供了高可用性。如果数据库的存储能力不够，数据量太大，则只需要加TiKV节点即可进行扩容。TiKV实现了分布式事务和MVCC。

**TiFlash**，用于存储数据，与TiKV的Region数据一致，采用列式存储，适合用于统计分析。

**PD**，是TiDB的大脑，为TiDB集群提供元数据和时间戳。在PD中存放着数据的元数据信息，TiDB Server在解析、优化SQL的时候，会从PD中获取Region与TiKV的元数据信息。PD还提供时间戳标识TSO，每条SQL执行都会获得一个TSO。在事务中，事务开始和事务提交都会获取一个TSO。

## TiDB Server

功能：

* 处理客户端连接
* SQL语句的解析和编译
* 关系型数据与KV数据的转化
* SQL语句的执行
* 执行online DDL
* 执行垃圾回收

![image-20230128230109724](images/image-20230128230109724.png)

TiDB Server接收用户会话请求，接收用户SQL并进行解析、编译、优化。由于TiKV是存储的K-V型数据，所以TiDB Server还负责将用户提交的关系型数据转化成K-V型数据。TiKV中存储的数据是保留每次变更的版本的，当版本数据过多，数据就会产生垃圾，TiDB Server还负责垃圾回收，默认每10分钟执行一次。

## TiKV

功能：

* 数据持久化
* 副本的强一致性和高可用性
* MVCC（多版本并发控制）
* 分布式事务支持
* 算子下推（Coprocessor），是一个分布式计算的模型

![image-20230128230655786](images/image-20230128230655786.png)

TiKV底层是使用RocksDB实现单机节点的K-V型数据的持久化。RocksDB kv是存储的K-V型数据，RocksDB raft存放的是数据从操作指令，即数据操作的增删改查指令。

RocksDB的上一层是Raft协议，Raft协议主要用来实现Region的多副本，一个Region的多个副本中，只有一个是leader角色，其他副本与leader保持同步。

Raft协议的上一层是MVCC，主要用来实现版本控制。

MVCC的上一层是分布式事务层，采用两阶段提交，让TiDB集群实现了事务的功能。

## Placement Driver，PD

功能：

* 整个集群TiKV的元数据存储
* 分配全局ID和事务ID
* 生成全局TSO
* 收集集群信息进行调度
* 提供Dashboard服务

![image-20230128231443448](images/image-20230128231443448.png)

TiKV会定时向PD汇报节点信息，PD会根据汇报信息实现数据调度，防止数据倾斜。

## TiFlash

功能：

* 异步复制
* 一致性
* 列式存储提高分析查询效率
* 业务隔离
* 智能选择

![image-20230128232010754](images/image-20230128232010754.png)

TiKV中存储的数据是按行存储的，适合交易场景，主要用于OLTP。

TiFlash中存储的数据与TiKV的数据一致，只是TiFlash是按列存储，适合分析型场景，主要用于OLAP。

TiDB Server的SQL优化器实现了智能选择，根据对SQL进行分析，判断是交易型SQL还是分析型SQL，智能选择数据是从TiKV读取还是从TiFlash读取。

# 知识点回顾

1. 下列功能是由 TiKV 或 TiFlash 实现的为？（ 选 2 项 ）

A. 根据集群中 Region 的信息，发出调度指令

B. 对于 OLAP 和 OLTP 进行业务隔离

C. 将关系型数据转化为 KV 存储进行持久化

D. 将 KV 存储转化为关系型数据返回给客户端

E. 配合 TiDB Server 生成事务的唯一 ID

F. 副本的高可用和一致性

解析：集群中发出调度指令的是PD；将关系型数据转化为 KV 存储在TiDB Server实现；将 KV 存储转化为关系型数据也是在TiDB Server实现；生成事务的唯一 ID是在PD实现的。



2. 关于 TiKV 或 TiDB Server，下列说法不正确的是？

 A. 数据被持久化在 TiKV 的 RocksDB 引擎中 

 B. 对于老版本数据的回收（GC），是由 TiDB Server 在 TiKV 上完成的 

 C. 两阶段提交的锁信息被持久化到 TiDB Server 中 

 D. Region 可以在多个 TiKV 节点上进行调度，但是需要 PD 节点发出调度指令 

解析：本题是反向选择，选择不正确的。TiDB Server不持久化数据的，两阶段提交的锁信息被持久化到TiKV中。
