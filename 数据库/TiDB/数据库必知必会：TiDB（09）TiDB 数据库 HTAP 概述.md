# HTAP技术

HTAP同时支持两种场景：

* OLTP
  * 支持实时更新的行存
  * 高并发，一致性要求高
  * 每次操作少量数据
* OLAP
  * 批量更新的列存
  * 并发数低
  * 每次操作大量数据

传统的架构如何同时支持OLTP和OLAP。

![image-20230204230209663](images/image-20230204230209663.png)

传统架构中，OLTP与OLAP的融合，需要借助ETL，会带来延迟，也就是 T+1。

传统架构中，还存在数据是多副本的问题。

# HTAP的要求

* 可扩展性
  * 分布式事务
  * 分布式存储
* 同时支持OLTP和OLAP
  * 同时支持行存和列存，并且数据保持一致
  * OLTP与OLAP的业务隔离
* 实时性
  * 行存与列存数据实时同步

# TiDB的HTAP

## 架构

![image-20230204231612248](images/image-20230204231612248.png)

在TiDB中加入了TiFlash，TiFlash是TiKV的准实时的复制版本，并且会将TiFlash转换成列存储。

TiDB实现了智能选择，事务型SQL读写TiKV，分析型SQL读写TiFlash。

## 特性

* 行列混合
  * 列存（TiFlash）支持基于主键的实时更新
  * TiFlash作为列存副本
  * OLTP与OLAP业务隔离
* 智能选择
* MPP架构

## MPP

特点：

* 大量数据的join聚合查询
* 所有MPP计算都在TiFlash节点内存中完成
* 目前仅支持等值连接

![image-20230204232309582](images/image-20230204232309582.png)

# 知识点回顾

1. 下面属于 HTAP 场景特点的是？（请选择 3 项）

A. 在故障恢复方面可以做到 RPO = 0

B. 支持分区特性

C. 支持在线业务高并发

D. 同时支持 OLTP 和 OLAP 业务

E. 能够读取到一致性的数据

解析：HTAP，实时性、可扩展、同时支持OLTP和OLAP



2. 关于 MPP 架构，下列说法不正确的是？

 A. MPP 架构的中间结果都在内存中 

 B. MPP 架构可以作用于 TiKV 和 TiFlash 上的数据 

 C. MPP 架构目前不支持非等值 join 

 D. MPP 架构可以对聚合、JOIN 等操作加速 

解析：本题选择不正确的选项，MPP架构是作用在TiFlash上的
