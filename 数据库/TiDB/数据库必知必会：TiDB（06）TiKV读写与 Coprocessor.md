# 读写与 Coprocessor

## 数据写入

![image-20230131234423745](images/image-20230131234423745.png)

用户提交写请求，由TiDB Server接收，TiDB Server向PD申请TSO，并获得Region的元数据信息，TiDB Server的写请求会由TiKV中的raftstore pool线程池接收并进行处理，执行Raft日志复制过程（Propose、Append、Replicate、Commited、Apply）完成数据写入操作。

## 数据读取

### Index Read

![image-20230131235613432](images/image-20230131235613432.png)

用户在读取数据的时候，读取请求会提交到TiDB Server，TiDB Server从PD获取数据的元数据信息，包括所在的TiKV、Region、Leader等等信息。

第1个问题是，TiDB Server拿到元数据后去TiKV节点找leader Region读取数据，此时不能完全保证在读取数据的时候原来的leader还是leader。因为这个时间间隔内，原leader可能失效，重新选举了leader，也就是TiDB Server从PD处拿到的leader信息在真正要读数据的这个时间间隔内失效了，变成follower了，就不能读数据了。要解决这个问题，TiDB采用了读取时进行心跳检测机制，TiDB Server拿到leader信息后，到leader region真正读数据的时候，由该region发起一次心跳检测，检测当前region是否还是leader，如果是，就直接读取数据，如果不是，就不能读取数据，重新获取leader region，从新的leader读取数据。

第2个问题是，抛开MVCC机制，只考虑Raft协议，用户的读操作需要读取之前的请求已经提交的数据。在读取时，如果有写操作存在，则读取被阻塞。什么时候可读？TiDB引入了ReadIndex和ApplyIndex。当读取数据时，先获取到要读取的数据所在的位置（index）。然后寻找Raft日志复制阶段中commited阶段的index，确保commited的index大于要读取的数据的index后，记录commited的index作为ReadIndex值，寻找Raft日志复制阶段中的apply阶段的index作为ApplyIndex值。只有当ApplyIndex的值等于ReadIndex的值时，可以确定当前index的数据已经提交持久化，从而知道要读取的数据所在的index的值已确定持久化，此时读取的数据就是提交后的数据。

![image-20230201003713644](images/image-20230201003713644.png)

### Lease Read

![image-20230202235101700](images/image-20230202235101700.png)

Lease Read也可以叫Local Read。

TiDB Server从PD获得leader时（TSO），此时的leader还是正常的，那么leader会发送心跳，时间间隔是heartbeat time interval，而follower会等待election timeout，如果达到election timeout还没收到心跳，才会进行重新选举leader。这意味着，即便从TiDB Server从PD获取到leader后（TSO后）leader就出现问题，那么也至少需要等待election timeout这么长时间集群才会重新选举leader，也就是在election timeout这个时间范围内，还是可以从原leader处读取数据的。这就是Lease Read。

### Follower Read

![image-20230203000353420](images/image-20230203000353420.png)

follower的数据与leader的数据是一致的，所以读实际上可以从follower读的，也就是读写分离是可以的，**前提是：**保证follower的数据与leader的数据是线性一致的。

要保证数据的线性一致，Follower Read是在follower节点上按照Index Read的方式读取数据的。先从leader节点获取到leader当前的commit index，然后需要follower节点的apply index等于获取到的commit index后才能进行数据的读取。

## Coprocessor

协同处理器，可以实现：执行物理算子，算子下推：聚合、全表扫描、索引扫描等。

比如，TiDB Server接收到用户的请求。

![image-20230203001732253](images/image-20230203001732253.png)

如果不引入Coprocessor机制，那么TiKV的所有数据会发送到TiDB Server做运算，一方面增加网络带宽，一方面TiDB Server的负载会很高。

![image-20230203001836218](images/image-20230203001836218.png)

引入Coprocessor后，可以实现count算子下推。

![image-20230203001921237](images/image-20230203001921237.png)

每个TiKV节点计算自己节点上的count，将值传到TiDB Server，TiDB Server只需要对各个节点的进行一次处理就好了。

![image-20230203001950205](images/image-20230203001950205.png)

# 知识点回顾

1. 下列属于 TiKV 相关功能的是？（ 选 4 项 ）

 A. 系统参数和元数据信息的持久化

B. 产生 TSO

C. 分布式事务实现

D. MVCC

E. 生成物理执行计划

F. 表统计信息的持久化

解析：TSO是由PD产生的，物理执行计划是由TiDB Server解析、优化SQL后生成的。



2. 关于 TiKV 数据持久化，下列说法不正确的是？

 A. RocksDB 有 2 个实例，分别用来持久化 raft log 和 key value 数据 

 B. RocksDB 中 WAL 用来保证写不丢失 

 C. 对于删除操作，只需要在原 key value 数据上标记已删除即可 

 D. RocksDB 中，除了 Level 0 层的数据，其他 Level 都是单一排序持久化的 

解析：本题选择不正确的，对于数据的变更，不会直接修改原数据，而是记录一条最新的操作记录即可。
