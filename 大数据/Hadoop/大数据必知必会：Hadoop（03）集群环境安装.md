# 安装前准备

集群环境下，至少需要3台服务器。

| IP地址   | 主机名称 |
| -------- | -------- |
| 10.0.0.5 | node1    |
| 10.0.0.6 | node2    |
| 10.0.0.7 | node3    |

需要保证每台服务器的配置都一致，以下步骤在3台服务器上都需要做一次。

## 操作系统准备

本次安装采用的操作系统是Ubuntu 20.04。

更新一下软件包列表。

```
sudo apt-get update
```

## 安装Java 8+

使用命令安装Java 8。

```
sudo apt-get install -y openjdk-8-jdk
```

配置环境变量。

```
vi ~/.bashrc

export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
```

让环境变量生效。

```
source ~/.bashrc
```

## 下载Hadoop安装包

从Hadoop官网[Apache Hadoop](https://hadoop.apache.org/releases.html)下载安装包软件。

![image-20230120200957218](images/image-20230120200957218.png)

或者直接通过命令下载。

```
wget https://dlcdn.apache.org/hadoop/common/hadoop-3.3.4/hadoop-3.3.4.tar.gz
```

![image-20230122004400490](images/image-20230122004400490.png)

# 分布式集群安装

分布式集群是在多个节点上运行进程来实现Hadoop集群。

## 配置域名解析

在后续使用过程中，都使用主机名称，所以需要配置域名解析。

配置 `/etc/hosts`。

> 由于该配置文件的修改需要root权限，所以在每个节点上都手动配置。

```
10.0.0.5 node1
10.0.0.6 node2
10.0.0.7 node3
```

>  **以下配置过程在node1上完成，并且配置完成后将配置文件复制到其他节点。**

## 配置免密登录

Hadoop分布式集群的运行，需要配置密钥对实现免密登录。

* 创建公私钥对

```
hadoop@node1:~$ ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/home/hadoop/.ssh/id_rsa): 
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/hadoop/.ssh/id_rsa
Your public key has been saved in /home/hadoop/.ssh/id_rsa.pub
The key fingerprint is:
SHA256:pp2AC1bQAQ5J6CJJCij1QA7bgKOsVxpoPVNi+cxhcyg hadoop@node1
The key's randomart image is:
+---[RSA 3072]----+
|O=*oo..          |
|OX E.* .         |
|X+* @ +          |
|B+.=.=           |
|= o++ . S        |
|..o. . = .       |
| .  . . o        |
|                 |
|                 |
+----[SHA256]-----+
```

* 复制公钥

```
hadoop@node1:~$ cp ~/.ssh/id_rsa.pub ~/.ssh/authorized_keys
```

* 复制到其他节点

```
hadoop@node1:~$ scp -r .ssh node1:~/
id_rsa.pub                                   100%  566     1.7MB/s   00:00    
authorized_keys                              100%  566     2.0MB/s   00:00    
known_hosts                                  100% 1332     4.5MB/s   00:00    
id_rsa                                       100% 2602    10.1MB/s   00:00    
hadoop@node1:~$ scp -r .ssh node2:~/
hadoop@node2's password: 
id_rsa.pub                                   100%  566   934.6KB/s   00:00    
authorized_keys                              100%  566   107.3KB/s   00:00    
known_hosts                                  100% 1332     2.5MB/s   00:00    
id_rsa                                       100% 2602     4.8MB/s   00:00    
hadoop@node1:~$ scp -r .ssh node3:~/
hadoop@node3's password: 
id_rsa.pub                                   100%  566     1.0MB/s   00:00    
authorized_keys                              100%  566     1.3MB/s   00:00    
known_hosts                                  100% 1332     2.8MB/s   00:00    
id_rsa                                       100% 2602     5.2MB/s   00:00    
```

确保执行ssh命令的时候不需要输入密码。

```
hadoop@node1:~$ ssh node1
hadoop@node1:~$ ssh node2
hadoop@node1:~$ ssh node3
```

## 解压安装包

将安装包解压到目标路径。

```
hadoop@node1:~$ mkdir -p apps
hadoop@node1:~$ tar -xzf hadoop-3.3.4.tar.gz -C apps
```

![image-20230122005658601](images/image-20230122005658601.png)

bin目录下存放的是Hadoop相关的常用命令，比如操作HDFS的hdfs命令，以及hadoop、yarn等命令。

etc目录下存放的是Hadoop的配置文件，对HDFS、MapReduce、YARN以及集群节点列表的配置都在这个里面。

sbin目录下存放的是管理集群相关的命令，比如启动集群、启动HDFS、启动YARN、停止集群等的命令。

share目录下存放了一些Hadoop的相关资源，比如文档以及各个模块的Jar包。

## 配置环境变量
在集群的每个节点上都配置Hadoop的环境变量，Hadoop集群在启动的时候可以使用start-all.sh一次性启动集群中的HDFS和Yarn，为了能够正常使用该命令，需要将其路径配置到环境变量中。

```
hadoop@node1:~$ vi ~/.bashrc

export HADOOP_HOME=/home/hadoop/apps/hadoop-3.3.4
export HADOOP_CONF_DIR=/home/hadoop/apps/hadoop-3.3.4/etc/hadoop
export YARN_CONF_DIR=/home/hadoop/apps/hadoop-3.3.4/etc/hadoop

export PATH=$HADOOP_HOME/bin:$HADOOP_HOME/sbin:$PATH
```
使环境变量生效。

```
hadoop@node1:~$ source ~/.bashrc
```



## 配置Hadoop集群

Hadoop软件安装完成后，每个节点上的Hadoop都是独立的软件，需要进行配置才能组成Hadoop集群。Hadoop的配置文件在$HADOOP_HOME/etc/hadoop目录下，主要配置文件有6个：

* hadoop-env.sh主要配置Hadoop环境相关的信息，比如安装路径、配置文件路径等；
* core-site.xml是Hadoop的核心配置文件，主要配置了Hadoop的NameNode的地址、Hadoop产生的文件目录等信息；
* hdfs-site.xml是HDFS分布式文件系统相关的配置文件，主要配置了文件的副本数、HDFS文件系统在本地对应的目录等；
* mapred-site.xml是关于MapReduce的配置文件，主要配置MapReduce在哪里运行；
* yarn-site.xml是Yarn相关的配置文件，主要配置了Yarn的管理节点ResourceManager的地址、NodeManager获取数据的方式等；
* workers是集群中节点列表的配置文件，只有在这个文件里面配置了的节点才会加入到Hadoop集群中，否则就是一个独立节点。

这几个配置文件如果不存在，可以通过复制配置模板的方式创建，也可以通过创建新文件的方式创建。需要保证在集群的每个节点上这6个配置保持同步，可以在每个节点单独配置，也可以在一个节点上配置完成后同步到其他节点。

### hadoop-env.sh配置

```
hadoop@node1:~$ vi $HADOOP_HOME/etc/hadoop/hadoop-env.sh

export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
export HADOOP_HOME=/home/hadoop/apps/hadoop-3.3.4
export HADOOP_CONF_DIR=/home/hadoop/apps/hadoop-3.3.4/etc/hadoop
export HADOOP_LOG_DIR=/home/hadoop/logs/hadoop
```

### core-site.xml配置

```
hadoop@node1:~$ vi $HADOOP_HOME/etc/hadoop/core-site.xml

<configuration>
    <property>
      <name>fs.defaultFS</name>
      <value>hdfs://node1:8020</value>
    </property>
    <property>
      <name>hadoop.tmp.dir</name>
      <value>/home/hadoop/data/hadoop/temp</value>
    </property>
    <property>
      <name>hadoop.proxyuser.hadoop.hosts</name>
      <value>*</value>
    </property>
    <property>
      <name>hadoop.proxyuser.hadoop.groups</name>
      <value>*</value>
    </property>
</configuration>
```

### hdfs-site.xml配置

```
hadoop@node1:~$ vi $HADOOP_HOME/etc/hadoop/hdfs-site.xml

<configuration>
    <property>
        <name>dfs.replication</name>
        <value>3</value>
    </property>
    <property>
      <name>dfs.namenode.name.dir</name>
      <value>/home/hadoop/data/hadoop/hdfs/name</value>
    </property>
    <property>
      <name>dfs.datanode.data.dir</name>
      <value>/home/hadoop/data/hadoop/hdfs/data</value>
    </property>
</configuration>
```

### mapred-site.xml配置

```
hadoop@node1:~$ vi $HADOOP_HOME/etc/hadoop/mapred-site.xml

<configuration>
    <property>
        <name>mapreduce.framework.name</name>
        <value>yarn</value>
    </property>
    <property>
        <name>mapreduce.application.classpath</name>
        <value>$HADOOP_HOME/share/hadoop/mapreduce/*:$HADOOP_HOME/share/hadoop/mapreduce/lib/*</value>
    </property>
</configuration>
```

### yarn-site.xml配置

```
hadoop@node1:~$ vi $HADOOP_HOME/etc/hadoop/yarn-site.xml

<configuration>
    <property>
      <name>yarn.nodemanager.aux-services</name>
      <value>mapreduce_shuffle</value>
    </property>
      <property>
        <name>yarn.resourcemanager.hostname</name>
        <value>node1</value>
    </property>
</configuration>
```

### workers配置

```
hadoop@node1:~$ vi $HADOOP_HOME/etc/hadoop/workers

node1
node2
node3
```

## 将软件及配置信息复制到其他节点

在node1上配置好环境变量及配置文件，可以手动再在其他节点上完成同样的配置，或者直接将node1的文件复制到其他节点。

```
hadoop@node1:~$ scp -r .bashrc apps node2:~/
hadoop@node1:~$ scp -r .bashrc apps node3:~/
```

## 格式化NameNode

在启动集群前，需要对NameNode进行格式化，在node1上执行以下命令：

```
hadoop@node1:~$ hdfs namenode -format
```

## 启动集群

在node1上执行start-all.sh命令启动集群。

```
hadoop@node1:~$ jps
55936 Jps
hadoop@node1:~$ start-all.sh
WARNING: Attempting to start all Apache Hadoop daemons as hadoop in 10 seconds.
WARNING: This is not a recommended production deployment configuration.
WARNING: Use CTRL-C to abort.
Starting namenodes on [node1]
Starting datanodes
node2: WARNING: /home/hadoop/logs/hadoop does not exist. Creating.
node3: WARNING: /home/hadoop/logs/hadoop does not exist. Creating.
Starting secondary namenodes [node1]
WARNING: YARN_CONF_DIR has been replaced by HADOOP_CONF_DIR. Using value of YARN_CONF_DIR.
Starting resourcemanager
WARNING: YARN_CONF_DIR has been replaced by HADOOP_CONF_DIR. Using value of YARN_CONF_DIR.
Starting nodemanagers
WARNING: YARN_CONF_DIR has been replaced by HADOOP_CONF_DIR. Using value of YARN_CONF_DIR.
node3: WARNING: YARN_CONF_DIR has been replaced by HADOOP_CONF_DIR. Using value of YARN_CONF_DIR.
node2: WARNING: YARN_CONF_DIR has been replaced by HADOOP_CONF_DIR. Using value of YARN_CONF_DIR.
node1: WARNING: YARN_CONF_DIR has been replaced by HADOOP_CONF_DIR. Using value of YARN_CONF_DIR.
hadoop@node1:~$ jps
57329 ResourceManager
57553 NodeManager
57027 SecondaryNameNode
58165 Jps
56437 NameNode
56678 DataNode
```

# 验证Hadoop

## 访问HDFS

上传一个文件到HDFS。

```
hdfs dfs -put .bashrc /
```

打开HDFS Web UI查看相关信息，默认端口9870。

![image-20230122011646516](images/image-20230122011646516.png)

![image-20230122011729528](images/image-20230122011729528.png)

![image-20230122011803241](images/image-20230122011803241.png)

## 访问YARN

打开YARN Web UI查看相关信息，默认端口8088。

![image-20230122011843198](images/image-20230122011843198.png)

# 相关命令

## HDFS相关的命令

操作HDFS使用的命令是hdfs，命令格式为：

```
Usage: hdfs [OPTIONS] SUBCOMMAND [SUBCOMMAND OPTIONS]
```

支持的Client命令主要有：

```
    Client Commands:

classpath            prints the class path needed to get the hadoop jar and the required libraries
dfs                  run a filesystem command on the file system
envvars              display computed Hadoop environment variables
fetchdt              fetch a delegation token from the NameNode
getconf              get config values from configuration
groups               get the groups which users belong to
lsSnapshottableDir   list all snapshottable dirs owned by the current user
snapshotDiff         diff two snapshots of a directory or diff the current directory contents with a snapshot
version              print the version
```

## YARN相关的命令

操作HDFS使用的命令是yarn，命令格式为：

```
Usage: yarn [OPTIONS] SUBCOMMAND [SUBCOMMAND OPTIONS]
 or    yarn [OPTIONS] CLASSNAME [CLASSNAME OPTIONS]
  where CLASSNAME is a user-provided Java class
```

支持的Client命令主要有：

```
    Client Commands:

applicationattempt   prints applicationattempt(s) report
app|application      prints application(s) report/kill application/manage long running application
classpath            prints the class path needed to get the hadoop jar and the required libraries
cluster              prints cluster information
container            prints container(s) report
envvars              display computed Hadoop environment variables
fs2cs                converts Fair Scheduler configuration to Capacity Scheduler (EXPERIMENTAL)
jar <jar>            run a jar file
logs                 dump container logs
nodeattributes       node attributes cli client
queue                prints queue information
schedulerconf        Updates scheduler configuration
timelinereader       run the timeline reader server
top                  view cluster information
version              print the version
```

yarn jar 可以执行一个jar文件。

* 验证案例1，统计含有“dfs”的字符串

创建一个input目录。

```
hadoop@node1:~$ hdfs dfs -mkdir /input
```

将Hadoop的配置文件复制到input目录下。

```
hadoop@node1:~$ hdfs dfs -put apps/hadoop-3.3.4/etc/hadoop/*.xml /input/
```

以下命令用于执行一个Hadoop自带的样例程序，统计input目录中含有dfs的字符串，结果输出到output目录。

```
hadoop@node1:~$ yarn jar $HADOOP_HOME/share/hadoop/mapreduce/hadoop-mapreduce-examples-3.3.4.jar grep /input /output 'dfs[a-z.]+'
```

![image-20230122012114789](images/image-20230122012114789.png)

在YARN上可以看到提交的Job。

![image-20230122012159358](images/image-20230122012159358.png)

执行结果为：

```
hadoop@node1:~$ hdfs dfs -cat /output/*
1       dfsadmin
1       dfs.replication
1       dfs.namenode.name.dir
1       dfs.datanode.data.dir
```

* 验证案例2，计算圆周率

同样执行Hadoop自带的案例，计算圆周率。

```
hadoop@node1:~$ yarn jar $HADOOP_HOME/share/hadoop/mapreduce/hadoop-mapreduce-examples-3.3.4.jar pi 10 10
```

执行结果为：

```
hadoop@node1:~$ yarn jar $HADOOP_HOME/share/hadoop/mapreduce/hadoop-mapreduce-examples-3.3.4.jar pi 10 10
WARNING: YARN_CONF_DIR has been replaced by HADOOP_CONF_DIR. Using value of YARN_CONF_DIR.
Number of Maps  = 10
Samples per Map = 10
Wrote input for Map #0
Wrote input for Map #1
Wrote input for Map #2
Wrote input for Map #3
Wrote input for Map #4
Wrote input for Map #5
Wrote input for Map #6
Wrote input for Map #7
Wrote input for Map #8
Wrote input for Map #9
Starting Job
... ...
Job Finished in 43.014 seconds
Estimated value of Pi is 3.20000000000000000000
```

在YARN上可以看到提交的Job。

![image-20230122012444050](images/image-20230122012444050.png)
