# HBase从C50迁移到6.5.1（03）6.5.1上准备Loader

在FusionInsight HD集群的HBase数据迁移过程中，需要通过Loader将老集群FusinInsight C50上获取HBase的数据加载到新集群FusionInsight 6.5.1，因此需要在新集群FusionInsight 6.5.1中添加Loader以及具有相应权限的用户loaderUser。

本文主要介绍如何在新集群FusionInsight 6.5.1中创建loaderUser用户以及如何添加配置Loader。

## 登录新集群FusionInsight 6.5.1的Manager

使用admin用户登录到需要装载数据的新集群FusionInsight 6.5.1的Manager。

![](images/image-20230818094243230.png)

登录成功后进入到Manager的主界面。

![](images/image-20230818094246231.png)

## 准备Loader服务

在集群菜单下，点击服务菜单，跳转到服务列表界面。

![](images/image-20230818094301232.png)

在服务列表界面，点击添加服务按钮，进行服务的添加。

![](images/image-20230818094316233.png)

由于数据迁移仅需要添加Loader服务，而不需要添加新的集群节点，所以在添加服务界面选择仅添加服务。点击下一步按钮进行下一步。

![](images/image-20230818094347234.png)

在步骤2，选择服务步骤中，选择需要添加的新服务，Loader服务。

![](images/image-20230818094402235.png)

其余不需要添加的服务保持未选中状态。点击下一步按钮进行下一步。

![](images/image-20230818094417236.png)

在步骤3，设置服务名称步骤中，设置添加后的服务的名称，这里保持默认的服务名称Loader即可。点击下一步按钮进入下一步。

![](images/image-20230818094417237.png)

在步骤5，分配角色步骤中，为Loader服务分配两个节点。点击下一步按钮进入下一步。

![](images/image-20230818094447238.png)

在步骤6，配置步骤中，通过配置项loader.float.ip为Loader配置一个唯一且未被使用的业务浮动IP。点击下一步按钮进入下一步。

![](images/image-20230818104922240.png)

在步骤7，确定步骤中，核实Loader服务的详细配置信息，核实无误后，点击提交按钮，提交Loader服务的添加。

![](images/image-20230818104937241.png)

提交以后，将进行请求参数校验。

![](images/image-20230818104952242.png)

请求参数校验通过后，将进入安装、配置、启动流程。

![](images/image-20230818105053243.png)

安装启动完成后，点击完成按钮结束Loader服务的添加流程。

![](images/image-20230818105409244.png)

Loader服务添加完成后，在服务列表界面可以看到新添加的Loader服务，状态正常。

![](images/image-20230818105439245.png)

至此，Loader服务添加完成。

## 准备Loader Role

在系统菜单下，通过权限下的角色菜单，打开角色管理界面，进行角色管理。在角色管理界面，点击添加角色按钮，进行角色的添加。

![](images/image-20230818092918340.png)

在添加角色界面，输入：角色名称loaderRole，并赋予目标集群的Loader的管理员权限。点击确定按钮以创建角色。

![](images/image-20230818095529341.png)

添加完角色后，在角色管理界面可以看到新添加的角色。

![](images/image-20230818095700342.png)

至此，角色添加完成。

## 准备Loader User

在系统菜单下，通过权限下的用户菜单，打开用户管理界面，进行用户管理。在用户管理界面，点击添加用户按钮，进行用户的添加。

![](images/image-20230818100217450.png)

在用户信息录入界面，输入：用户名loaderUser，用户类型人机，用户密码loaderUser@123，用户组设置为hadoop、supergroup，主组设置为hadoop，其余信息可以不填写。用户信息输入完成后，点击确定按钮进行用户创建的提交。

![](images/image-20230818100819451.png)

用户添加完成以后，需要到安装有FusionInsight HD的集群客户端的服务器上，验证新添加的用户loaderUser是否可以正常认证、登录。

执行命令：

```shell
source /opt/hadoopclient/bigdata_env
kinit loaderUser
```

输入用户密码，进行认证，初次登录认证需要修改密码，可以将密码修改为loaderUser@1234：

```shell
Password for loaderUser@HADOOP.COM: 
Password expired.  You must change it now.
Enter new password: 
Enter it again:
```

认证成功，则loaderUser用户创建成功。
