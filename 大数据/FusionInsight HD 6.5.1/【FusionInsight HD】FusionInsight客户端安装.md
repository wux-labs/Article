# FusionInsight HD 6.5.1客户端安装

## 客户端下载

首先，打开FusionInsight Manager的Web界面，使用admin用户登录管理控制台。

![image-20230713142229952](images/image-20230713142229952.png)

通过主页界面的集群管理菜单下的【下载客户端】菜单，下载FusionInsight集群的客户端。

![image-20230713142336672](images/image-20230713142336672.png)

在弹出的“下载集群客户端”信息提示框中，设置：

* 选择客户端类型：选择“完整客户端”。

> “仅配置文件”下载的客户端配置文件，仅包含配置文件，不包含安装软件。
>
> 适用于：客户端已经安装成功后，管理员修改了服务端的配置，需要更新客户端的配置文件的场景。

* 选择平台类型：选择x86_64

> x86_64，可以部署在x86平台上。
>
> aarch64，可以部署在鲲鹏专属机或者TaiShan服务器。

* 勾选仅保存到如下路径，并输入需要保存的路径

> 如果不勾选，客户端会直接下载到本地，然后再从本地复制到需要安装客户端的服务器。
>
> 如果勾选，客户端不会下载到本地，而是存放在指定路径，可以通过cp、scp等命令直接复制到需要安装客户端的服务器。
>
> 考虑到客户端软件的大小在2GB以上，下载到本地再上传会比较慢，直接用scp命令在服务器节点之间复制会很快，所以勾选仅保存到如下路径。

配置完成后，点击“确定”按钮下载集群客户端。

![image-20230713142435137](images/image-20230713142435137.png)

客户端下载完成后，会提示详细信息，包括下载是否成功、服务器路径等。关闭对话框即可。

![image-20230713142757840](images/image-20230713142757840.png)

到客户端下载路径，可以看到客户端已经存放在指定的路径了，大小在2GB以上。

![image-20230713142840615](images/image-20230713142840615.png)

## 客户端安装

### 解压软件

将下载好的客户端，通过scp命令复制到需要安装客户端的服务器上。

```bash
scp FusionInsight_Cluster_1_Services_Client.tar root@10.16.35.34:/opt/
```

![image-20230713143228711](images/image-20230713143228711.png)

客户端复制完成后，登录到需要安装客户端的服务器，对客户端软件包FusionInsight_Cluster_1_Services_Client.tar进行解包。

```bash
 tar -xvf FusionInsight_Cluster_1_Services_Client.tar 
```

解包后会得到两个文件：

* FusionInsight_Cluster_1_Services_ClientConfig.tar，这个是软件安装包文件
* FusionInsight_Cluster_1_Services_ClientConfig.tar.sha256，这个是软件安装包的校验文件

![image-20230713144031565](images/image-20230713144031565.png)

解包完成后，使用sha256sum命令校验解压文件，看看文件是否正确。

> 一般自己下载的客户端，校验都是没问题的，所以这一步可做可不做。

```bash
sha256sum -c FusionInsight_Cluster_1_Services_ClientConfig.tar.sha256
```

![image-20230713145835422](images/image-20230713145835422.png)

文件校验成功后，对客户端软件包FusionInsight_Cluster_1_Services_ClientConfig.tar进行解包。

```bash
 tar -xvf FusionInsight_Cluster_1_Services_ClientConfig.tar
 ls -al
```

解包完成后，会得到软件安装包文件夹FusionInsight_Cluster_1_Services_ClientConfig，里面包含安装客户端所需的所有文件。

![image-20230713145949958](images/image-20230713145949958.png)

### 配置网络

需要确保客户端所在的主机能够与集群中的所有主机在网络上互通，需要配置客户端到服务器节点的域名解析。在软件安装包文件夹下的hosts文件中包含了集群中的所有主机信息，直接将这个文件的内容追加到/etc/hosts文件中即可。

> 当客户端所在主机不是集群中的节点时，需要在客户端所在主机的/etc/hosts文件中设置：
>
> * **集群所有节点主机名**：在软件安装包文件夹下的hosts文件中包含了集群中的所有主机信息
> * **业务平面IP地址映射**：在软件安装包文件夹下的realm.ini文件中包含了业务平面IP地址映射信息
>
> 将这两个文件的内容都追加到客户端主机的/etc/hosts文件中。

```bash
cat hosts >> /etc/hosts
cat realm.ini >> /etc/hosts
```

![image-20230713150548637](images/image-20230713150548637.png)

### 安装软件

进入安装包所在的目录后，执行以下命令安装客户端到指定的目录，比如安装到”/opt/hadoopclient“目录。

```bash
./install.sh /opt/hadoopclient
```

![image-20230713150741773](images/image-20230713150741773.png)

安装完成后，日志中可以看到组件安装完成、注册到OM中。

![image-20230713150901748](images/image-20230713150901748.png)

注册完成后，在FusionInsight Manager界面中可以看到安装成功并注册成功的客户端信息。

![image-20230713151422202](images/image-20230713151422202.png)

### 验证安装

安装完成后，执行以下步骤可验证客户端是否安装成功。

* 执行cd /opt/hadoopclient进入客户端安装目录

* 执行source bigdata_env配置客户端环境变量
* 执行kinit命令设置kinit命令
* 执行klist命令查询并确认权限内容

![image-20230713151016918](images/image-20230713151016918.png)

认证成功后，klist命令可以查看到当前认证的用户（admin），认证的有效期等。

![image-20230713151309756](images/image-20230713151309756.png)

## 添加认证用户

### 下载用户凭据

通过FusionInsight Manager界面，【系统】->【权限】->【用户】菜单，找到用户列表中需要添加客户端认证的用户，通过【更多】菜单下的【下载认证凭据】下载用户的认证凭据。

![image-20230713152804401](images/image-20230713152804401.png)

在弹出的”下载认证凭据“对话框中，选择当前集群，点击”确定“按钮下载凭据。

![image-20230713152819829](images/image-20230713152819829.png)

下载的用户认证凭据包括krb5.conf和user.keytab两个文件。

![image-20230713153418842](images/image-20230713153418842.png)

### 上传用户凭据

将压缩包中的两个文件复制上传到客户端安装目录/opt/hadoopclient中，并修改文件的权限及所属用户、组信息等。

```bash
chown root:root krb5.conf user.keytab 
chmod 777 krb5.conf user.keytab
```

![image-20230713154126310](images/image-20230713154126310.png)

### 验证用户认证

在客户端主机执行kinit命令使用新添加的用户进行认证，认证成功后可以通过klist命令查看并确认认证权限内容。

```bash
kinit -kt user.keytab crm
```

![image-20230713154240882](images/image-20230713154240882.png)

## 客户端验证

### Hive操作验证

使用beeline命令验证通过客户端操作Hive数据仓库。

```bash
beeline
```

可以通过show tables语句查看当前Hive仓库中的表。

> 因为是全新的环境，所以目前没有表。

![image-20230713154424487](images/image-20230713154424487.png)

### HBase操作验证

通过sqlline.py命令验证通过客户端操作Phoenix/HBase。

```bash
sqlline.py
```

可以通过!tables命令查看当前HBase中已有的表。

> 因为是全新的环境，所以在初次操作的时候会自动创建系统相关的、元数据相关的一些表。
>
> !tables 命令可以查看这些新建的表。

![image-20230713154710648](images/image-20230713154710648.png)

### Spark作业提交验证

通过spark-submit命令验证通过客户端提交Spark作业到集群运行。

```bash
 sh ~/spark_job/all/bin/run_job_by_conf.sh dev test_spark_etl
```

![image-20230713161450593](images/image-20230713161450593.png)

Spark作业作业提交集群运行后，可以通过Yarn Web UI查看当前提交的作业信息、运行情况及查看日志等。

![image-20230713161524108](images/image-20230713161524108.png)

## 总结

至此，客户端安装完成。

我们实现了：客户端安装、自定义用户认证、客户端验证等操作。
