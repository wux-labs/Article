# FusionInsight HD 6.5.1安装集群

Fusionlnsight HD是华为开发的大数据平台，基于开源社区软件进行功能增强，对外提供大容量的数据存储、查询和分析能力，可运行在X86服务器或华为TaiShan服务器上。

## 安装方案

### 组网方案

FusionInsight HD集群网络分为两个平面：

* 管理平面：主要用于集群管理，对外提供集群监控、配置、审计、用户管理等服务。
* 业务平面：主要为用户或上层用户提供业务通道，对外提供数据存储、业务提交和计算的功能。

FusionInsight HD集群中包含的节点：

* 管理节点（MN）：安装FusionInsight Manager，提供统一的运维管理界面入口。
* 控制节点（CN）：控制监控数据节点执行存储数据、接收数据、发送进程状态以及完成控制节点的公共功能。
* 数据节点（DN）：执行管理节点发出的指示，上报任务状态、存储数据以及执行数据节点的公共功能。

### 部署方案

FusionInsight HD集群的部署方案有：

* MN、CN、DN分开部署：节点数30-5000时，根据不同场景分配。
* MN与CN合并部署、DN独立部署：数据节点30-100时采用此方案。
* MN、CN、DN合并部署：节点数小于6的集群使用此方案，最低需要3个节点。

> 由于集群对节点的配置有一定的要求，笔者没有这么多节点可用，因此本文采用最低配置进行安装，仅用3个节点进行。
>
> * node1：10.0.5.31
> * node2：10.0.5.32
> * node3：10.0.5.33

## 安装Manager

### 软件准备

将集群安装软件包上传到主管理节点。

解压Manager和SetupTool到`/opt`目录下。

```bash
tar -xzf FusionInsight_Manager_6.5.1_RHEL.tar.gz -C /opt/
tar -xzf FusionInsight_SetupTool_6.5.1.7.tar.gz -C /opt/
```

![image-20230715005650031](images/image-20230715005650031.png)

将安装软件复制到`/opt/FusionInsight_Manager/software/packs`目录下。

```bash
cp FusionInsight_BASE_6.5.1_RHEL.tar.gz FusionInsight_Elasticsearch_6.5.1_RHEL.tar.gz FusionInsight_Flink_6.5.1_RHEL.tar.gz FusionInsight_HD_6.5.1_RHEL.tar.gz FusionInsight_Porter_6.5.1_RHEL.tar.gz FusionInsight_Spark2X_6.5.1_RHEL.tar.gz /opt/FusionInsight_Manager/software/packs
```

![image-20230715005903093](images/image-20230715005903093.png)

将操作系统镜像挂载到`/media/`目录。

```bash
mount /dev/cdrom /media/
```

![image-20230715005959762](images/image-20230715005959762.png)

### 生成配置

打开FusionInsight 6.5.1 配置规划工具，启用宏，语言选择"中文"后点击开始使用。

> 根据自定义情况填写各项配置。

#### 工具说明

选择中文、版本6.5.1，开始使用。

![image-20230714230620370](images/image-20230714230620370.png)

#### 基础配置

设置集群名称、版本、安装路径、数据路径、用户等。

![image-20230714230822805](images/image-20230714230822805.png)

设置安装方案、配置输出路径、配置上传路径等。

![image-20230714231001227](images/image-20230714231001227.png)

#### 选择服务

选择需要安装的服务，这里选择最小服务Yarn。

![image-20230714231129029](images/image-20230714231129029.png)

#### IP规划与进程部署

设置3个节点信息，设置每个节点需要安装的服务。

![image-20230714231334403](images/image-20230714231334403.png)

#### 节点信息

设置节点信息，设置内存、磁盘、主机名等。

![image-20230714231516393](images/image-20230714231516393.png)

#### 浮动IP配置

设置浮动IP。

![image-20230714231824503](images/image-20230714231824503.png)

#### 磁盘配置

设置磁盘信息。

![image-20230714231909410](images/image-20230714231909410.png)

#### 生成配置文件

配置完成后，即可生成配置文件。

![image-20230714232159048](images/image-20230714232159048.png)

### 配置并检查安装环境

在安装之前，先检查一下是否满足安装要求。

```bash
cd /opt/FusionInsight_SetupTool/preinstall/
cat preinstall.ini
```

![image-20230715010736325](images/image-20230715010736325.png)



安装前的系统情况。

![image-20230715010946705](images/image-20230715010946705.png)

```bash
cd /opt/FusionInsight_SetupTool
./setuptool.sh precheck
```

检查没问题，执行环境配置。

```bash
./setuptool.sh preinstall
```

环境配置完成后，可以看到各个节点的磁盘、网络等均已配置好。

![image-20230715012906677](images/image-20230715012906677.png)

### 安装Manager

由于只有3个节点，没有必要安装双机Manager，因此选择安装单机Manager。

> 需要编辑配置文件，将双机Manager改成单机Manager。

![image-20230715013031532](images/image-20230715013031532.png)

```bash
cd /opt/FusionInsight_Manager/software
vi install_oms/10.0.5.31.ini
```

![image-20230715013117212](images/image-20230715013117212.png)

配置文件修改完成后，执行单机Manager的安装。

```bash
./install.sh -f /opt/FusionInsight_Manager/software/install_oms/10.0.5.31.ini
```

![image-20230716005214794](images/image-20230716005214794.png)

安装完成后，可以访问FusionInsight Manager界面

https://10.0.5.41:8080/web

![image-20230716005344743](images/image-20230716005344743.png)

![image-20230716005557372](images/image-20230716005557372.png)

## 总结

至此，Manager安装完成。

我们实现了：安装方案规划、集群配置规划、Manager安装。
