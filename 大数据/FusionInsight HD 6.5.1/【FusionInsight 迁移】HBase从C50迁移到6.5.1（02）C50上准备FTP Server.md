# HBase从C50迁移到6.5.1（02）C50上准备FTP Server

在FusionInsight HD集群的HBase数据迁移过程中，需要通过FTP-Server从老集群FusinInsight C50上获取HBase的数据，因此需要在老集群FusinInsight C50中添加FTP-Server以及具有相应权限的用户ftpUser。

本文主要介绍如何在老集群FusinInsight C50中创建ftpUser用户以及如何添加配置FTP-Server。

## 登录老集群FusionInsight C50的Manager

使用admin用户登录到需要迁移数据的老集群FusinInsight C50的Manager。

![](images/image-20230818091457123.png)

登录成功后进入到Manager的主界面。

![](images/image-20230818091557124.png)

## 准备FTP User

在System菜单下，通过Rights Configuration下的User Management连接，进行用户管理。

![](images/image-20230818091728125.png)

在用户管理界面，点击Add User按钮进行添加用户。

![](images/image-20230818091858126.png)

在用户信息录入界面，输入：用户名ftpUser，用户类型Hunman-Machine（人-机），用户密码ftpUser@123，用户组设置为hadoop、hbase、supergroup，主组设置为hadoop，其余信息可以不填写。用户信息输入完成后，点击OK按钮进行用户创建的提交。

![](images/image-20230818092058127.png)

在确认界面，点击OK进行确认。

![](images/image-20230818092143128.png)

用户添加完成以后，需要到安装有FusionInsight HD的集群客户端的服务器上，验证新添加的用户ftpUser是否可以正常认证、登录。

执行命令：

```shell
source /opt/hadoopclient/bigdata_env
kinit ftpUser
```

输入用户密码，进行认证，初次登录认证需要修改密码，可以将密码修改为ftpUser@1234：

```shell
Password for ftpUser@HADOOP.COM: 
Password expired.  You must change it now.
Enter new password: 
Enter it again:
```

认证过程如下：

![](images/image-20230818092243129.png)

认证成功，则ftpUser用户创建成功。

## 准备FTP Server

在Services菜单下，服务管理界面中，点击Add Service按钮，进行服务的添加。

![](images/image-20230818103147130.png)

在步骤1，Select Step步骤中，选择Only Add Service，因为这里只需要添加一个服务，而不需要增加主机节点。点击Next按钮进入下一步。

![](images/image-20230818103202131.png)

在步骤4，Select Service步骤中，选择需要添加的FTP-Server服务。点击Next按钮进入下一步。

![](images/image-20230818103217132.png)

在步骤5，Assign Roles步骤中，为新添加的FTP-Server指定一台主机。点击Next按钮进入下一步。

![](images/image-20230818103317133.png)

在步骤6，Configuration步骤中，配置FTP-Server的配置项：ftp-active-enabled设置为true，ftp-enabled设置为true，ftps-active-enabled设置为false，ftps-enabled设置为true。点击Next按钮进入下一步。

![](images/image-20230818103417134.png)

在步骤7，Confirm步骤中，核实添加的FTP-Server的详细信息，核实无误后点击Submit进行提交。

![](images/image-20230818103517135.png)

提交后，将进入安装、配置、启动流程。

![](images/image-20230818103547136.png)

安装流程执行完成后，点击Finsh按钮结束安装。

![](images/image-20230818103632137.png)

安装完成后，可以通过Services界面看到新安装的FTP-Server服务，其状态正常、健康状况健康，配置信息已同步，服务正常。

![](images/image-20230818103703138.png)

至此，FTP-Server安装成功。
