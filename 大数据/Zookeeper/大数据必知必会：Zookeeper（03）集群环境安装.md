# 安装前准备

集群环境下，至少需要3台服务器。

| IP地址   | 主机名称 |
| -------- | -------- |
| 10.0.0.5 | node1    |
| 10.0.0.6 | node2    |
| 10.0.0.7 | node3    |

需要保证每台服务器的配置都一致，以下步骤在3台服务器上都需要做一次。

## 操作系统准备

本次安装采用的操作系统是Ubuntu 20.04。

更新软件包列表。

```
sudo apt-get update
```

## 安装Java 8+

使用命令安装Java 8。

```
sudo apt-get install -y openjdk-8-jdk
```

配置环境变量。

```
vi .bashrc

export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
```

让环境变量生效。

```
source .bashrc
```

## 下载Zookeeper安装包

从Zookeeper官网[Apache ZooKeeper](https://zookeeper.apache.org/releases.html)下载安装包软件。

![image-20230117144145227](images/image-20230117144145227.png)

或者直接通过命令下载。

```
wget https://dlcdn.apache.org/zookeeper/zookeeper-3.8.0/apache-zookeeper-3.8.0-bin.tar.gz
```

![image-20230119171254429](images/image-20230119171254429.png)

# 集群环境安装

## 解压安装包

在3台服务器上，分别将安装包解压到目标路径。

```
mkdir -p apps
tar -xzf apache-zookeeper-3.8.0-bin.tar.gz -C apps
```

![image-20230119171424505](images/image-20230119171424505.png)

## 修改配置文件

Zookeeper的配置文件保存在`$ZOO_HOME/conf/zoo.cfg`。

在3台服务器上都复制一份配置文件。

```
cp apps/apache-zookeeper-3.8.0-bin/conf/zoo_sample.cfg apps/apache-zookeeper-3.8.0-bin/conf/zoo.cfg
```

配置文件说明：

```
# The number of milliseconds of each tick
# 通信心跳时间，Zookeeper服务器与客户端心跳时间，单位毫秒。
tickTime=2000

# The number of ticks that the initial 
# synchronization phase can take
# LF初始通信时限
# Leader和Follower初始连接时能容忍的最多心跳数，单位次（即tickTime的数量）
initLimit=10

# The number of ticks that can pass between 
# sending a request and getting an acknowledgement
# LF同步通信时限
# Leader和Follower连接之后，通信时能容忍的最多心跳数，单位次
# 时间如果超过syncLimit * tickTime，Leader认为Follwer挂掉，从服务器列表中删除Follwer
syncLimit=5

# the directory where the snapshot is stored.
# do not use /tmp for storage, /tmp here is just 
# example sakes.
# Zookeeper数据存放目录
dataDir=/home/hadoop/data/zookeeper

# the port at which the clients will connect
# 客户端连接端口，通常不做修改
clientPort=2181
```

在集群模式下，还有一个重要的配置项，`server.A=B:C:D`，其中 A 是一个数字，表示这个是第几号服务器；B 是这个服务器的IP地址；C 是这个服务器与集群中的 Leader 服务器交换信息的端口；D 是集群中的 Leader 服务器挂了之后重新进行选举新的 Leader时服务器相互通信的端口。由于集群环境的IP地址不同，所以3台服务器的配置文件可以保持完全相同。

在3台服务器上都配置为：

```
# Zookeeper数据存放目录
dataDir=/home/hadoop/data/zookeeper
# 客户端连接端口
clientPort=2181
# 服务器节点配置
server.1=10.0.0.5:8881:7771
server.2=10.0.0.6:8881:7771
server.3=10.0.0.7:8881:7771
```

在3台服务器上都创建数据存放目录。

```
mkdir -p /home/hadoop/data/zookeeper
```

在 dataDir 指定的目录下创建名为 myid 的文件，文件内容和 `zoo.cfg `中`server.A=B:C:D`的 A 一致。

* node1上执行

```
echo 1 > /home/hadoop/data/zookeeper/myid
```

![image-20230119172642034](images/image-20230119172642034.png)

* node2上执行

```
echo 2 > /home/hadoop/data/zookeeper/myid
```

![image-20230119172721860](images/image-20230119172721860.png)

* node3上执行

```
echo 3 > /home/hadoop/data/zookeeper/myid
```

![image-20230119172757728](images/image-20230119172757728.png)

# 相关命令

如果没配置环境变量，则需要切换到安装目录下执行相关命令，或者指定命令的绝对路径。

```
cd apps/apache-zookeeper-3.8.0-bin
```

## 启动Zookeeper

在集群环境下，需要在每台服务器上都启动Zookeeper。

```
bin/zkServer.sh start
```

* node1

![image-20230119173010219](images/image-20230119173010219.png)

* node2

![image-20230119173105315](images/image-20230119173105315.png)

* node3

![image-20230119173350726](images/image-20230119173350726.png)

QuorumPeerMain 就是 Zookeeper 服务端的进程。

## 查看状态

分别在每台服务器上查看状态。

```
bin/zkServer.sh status
```

* node1

![image-20230119173934241](images/image-20230119173934241.png)

* node2

![image-20230119174022914](images/image-20230119174022914.png)

* node3

![image-20230119174043822](images/image-20230119174043822.png)

Mode: leader 表示Leader进程。

Mode: follower 表示Follower进程。

## 验证Zookeeper

* 数据操作验证

连接集群，写入数据，查看数据，再切换到其他节点查看数据。

```
bin/zkCli.sh -server 10.0.0.5:2181
bin/zkCli.sh -server 10.0.0.6:2181
bin/zkCli.sh -server 10.0.0.7:2181
```

在节点1上写入数据。

![image-20230119174241655](images/image-20230119174241655.png)

在节点2上进行验证。

![image-20230119174330192](images/image-20230119174330192.png)

在节点3上进行验证。

![image-20230119174418151](images/image-20230119174418151.png)

* Leader重选验证

当前状态下节点2是Leader，停止节点2的进程，查看其他两个节点的状态。

```
bin/zkServer.sh stop
```

![image-20230119174529440](images/image-20230119174529440.png)

节点3变为新的Leader。

## 停止Zookeeper

分别在每台服务器上停止进程。

```
bin/zkServer.sh stop
```

