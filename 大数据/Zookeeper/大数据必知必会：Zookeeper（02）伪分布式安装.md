# 安装前准备

## 操作系统准备

本次安装采用的操作系统是Ubuntu 20.04。

更新软件包列表。

```
sudo apt-get update
```

## 安装Java 8+

使用命令安装Java 8。

```
sudo apt-get install -y openjdk-8-jdk
```

配置环境变量。

```
vi .bashrc

export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
```

让环境变量生效。

```
source .bashrc
```

## 下载Zookeeper安装包

从Zookeeper官网[Apache ZooKeeper](https://zookeeper.apache.org/releases.html)下载安装包软件。

![image-20230117144145227](images/image-20230117144145227.png)

或者直接通过命令下载。

```
wget https://dlcdn.apache.org/zookeeper/zookeeper-3.8.0/apache-zookeeper-3.8.0-bin.tar.gz
```

![image-20230117144432407](images/image-20230117144432407.png)

# 伪分布式安装

伪分布式是在单机环境下采用多个Zookeeper进程来模拟Zookeeper集群，集群中至少需要3个节点。

## 解压安装包

将安装包解压到目标路径。

```
mkdir -p apps
tar -xzf apache-zookeeper-3.8.0-bin.tar.gz -C apps
```

![image-20230117144728656](images/image-20230117144728656.png)

## 修改配置文件

Zookeeper的配置文件保存在`$ZOO_HOME/conf/zoo.cfg`。

伪分布式模式下需要3个节点，每个节点使用独立的配置文件，所以将配置文件复制3份。

```
cp apps/apache-zookeeper-3.8.0-bin/conf/zoo_sample.cfg apps/apache-zookeeper-3.8.0-bin/conf/zoo1.cfg
cp apps/apache-zookeeper-3.8.0-bin/conf/zoo_sample.cfg apps/apache-zookeeper-3.8.0-bin/conf/zoo2.cfg
cp apps/apache-zookeeper-3.8.0-bin/conf/zoo_sample.cfg apps/apache-zookeeper-3.8.0-bin/conf/zoo3.cfg
```

配置文件说明：

```
# The number of milliseconds of each tick
# 通信心跳时间，Zookeeper服务器与客户端心跳时间，单位毫秒。
tickTime=2000

# The number of ticks that the initial 
# synchronization phase can take
# LF初始通信时限
# Leader和Follower初始连接时能容忍的最多心跳数，单位次（即tickTime的数量）
initLimit=10

# The number of ticks that can pass between 
# sending a request and getting an acknowledgement
# LF同步通信时限
# Leader和Follower连接之后，通信时能容忍的最多心跳数，单位次
# 时间如果超过syncLimit * tickTime，Leader认为Follwer挂掉，从服务器列表中删除Follwer
syncLimit=5

# the directory where the snapshot is stored.
# do not use /tmp for storage, /tmp here is just 
# example sakes.
# Zookeeper数据存放目录
dataDir=/home/wux_labs/data/zookeeper

# the port at which the clients will connect
# 客户端连接端口，通常不做修改
clientPort=2181
```

在集群模式下，还有一个重要的配置项，`server.A=B:C:D`，其中 A 是一个数字，表示这个是第几号服务器；B 是这个服务器的IP地址；C 是这个服务器与集群中的 Leader 服务器交换信息的端口；D 是集群中的 Leader 服务器挂了之后重新进行选举新的 Leader时服务器相互通信的端口。伪分布式集群中，由于 B 都是一样，所以不同的 Zookeeper 实例通信端口号不能一样，要分配不同的端口号。

主要配置内容为：

* zoo1.cfg

```
# Zookeeper数据存放目录
dataDir=/home/wux_labs/data/zookeeper1
# 客户端连接端口
clientPort=2181
# 服务器节点配置
server.1=10.0.0.4:8881:7771
server.2=10.0.0.4:8882:7772
server.3=10.0.0.4:8883:7773
```

* zoo2.cfg

```
# Zookeeper数据存放目录
dataDir=/home/wux_labs/data/zookeeper2
# 客户端连接端口
clientPort=2182
# 服务器节点配置
server.1=10.0.0.4:8881:7771
server.2=10.0.0.4:8882:7772
server.3=10.0.0.4:8883:7773
```

* zoo3.cfg

```
# Zookeeper数据存放目录
dataDir=/home/wux_labs/data/zookeeper3
# 客户端连接端口
clientPort=2183
# 服务器节点配置
server.1=10.0.0.4:8881:7771
server.2=10.0.0.4:8882:7772
server.3=10.0.0.4:8883:7773
```

创建数据存放目录。

```
mkdir -p /home/wux_labs/data/zookeeper1
mkdir -p /home/wux_labs/data/zookeeper2
mkdir -p /home/wux_labs/data/zookeeper3
```

在 dataDir 指定的目录下创建名为 myid 的文件, 文件内容和 `zoo.cfg `中`server.A=B:C:D`的 A 一致。

```
echo 1 > /home/wux_labs/data/zookeeper1/myid
echo 2 > /home/wux_labs/data/zookeeper2/myid
echo 3 > /home/wux_labs/data/zookeeper3/myid
```

# 相关命令

如果没配置环境变量，则需要切换到安装目录下执行相关命令，或者指定命令的绝对路径。

```
cd apps/apache-zookeeper-3.8.0-bin
```

## 启动Zookeeper

由于伪分布式中的每个进程需要使用独立的配置文件，所以在启动进程的时候需要指定配置文件。

```
bin/zkServer.sh start zoo1.cfg
bin/zkServer.sh start zoo2.cfg
bin/zkServer.sh start zoo3.cfg
```

![image-20230118103056740](images/image-20230118103056740.png)

QuorumPeerMain 就是 Zookeeper 服务端的进程。

## 查看状态

与启动进程一样，查看状态也需要指定配置文件。

```
bin/zkServer.sh status zoo1.cfg
bin/zkServer.sh status zoo2.cfg
bin/zkServer.sh status zoo3.cfg
```

![image-20230118103300042](images/image-20230118103300042.png)

Mode: leader 表示Leader进程。

Mode: follower 表示Follower进程。

## 验证Zookeeper

* 数据操作验证

连接集群，写入数据，查看数据，再切换到其他节点查看数据。

```
bin/zkCli.sh -server 10.0.0.4:2181
bin/zkCli.sh -server 10.0.0.4:2182
bin/zkCli.sh -server 10.0.0.4:2183
```

在节点1上写入数据。

![image-20230118103906754](images/image-20230118103906754.png)

在节点2上进行验证。

![image-20230118104002061](images/image-20230118104002061.png)

在节点3上进行验证。

![image-20230118104048403](images/image-20230118104048403.png)

* Leader重选验证

当前状态下节点2是Leader，停止节点2的进程，查看其他两个节点的状态。

```
bin/zkServer.sh stop zoo2.cfg
```

![image-20230118104334622](images/image-20230118104334622.png)

节点3变为新的Leader。

## 停止Zookeeper

与启动进程一样，停止进程也需要指定配置文件。

```
bin/zkServer.sh stop zoo1.cfg
bin/zkServer.sh stop zoo2.cfg
bin/zkServer.sh stop zoo3.cfg
```

