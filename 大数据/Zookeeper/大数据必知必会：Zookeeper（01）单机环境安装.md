# 安装前准备

## 操作系统准备

本次安装采用的操作系统是Ubuntu 20.04。

更新软件包列表。

```
sudo apt-get update
```

## 安装Java 8+

使用命令安装Java 8。

```
sudo apt-get install -y openjdk-8-jdk
```

配置环境变量。

```
vi .bashrc

export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
```

让环境变量生效。

```
source .bashrc
```

## 下载Zookeeper安装包

从Zookeeper官网[Apache ZooKeeper](https://zookeeper.apache.org/releases.html)下载安装包软件。

![image-20230117144145227](images/image-20230117144145227.png)

或者直接通过命令下载。

```
wget https://dlcdn.apache.org/zookeeper/zookeeper-3.8.0/apache-zookeeper-3.8.0-bin.tar.gz
```

![image-20230117144432407](images/image-20230117144432407.png)

# 单机环境安装

## 解压安装包

将安装包解压到目标路径。

```
mkdir -p apps
tar -xzf apache-zookeeper-3.8.0-bin.tar.gz -C apps
```

![image-20230117144728656](images/image-20230117144728656.png)

## 修改配置文件

Zookeeper的配置文件保存在`$ZOO_HOME/conf/zoo.cfg`。

```
cp apps/apache-zookeeper-3.8.0-bin/conf/zoo_sample.cfg apps/apache-zookeeper-3.8.0-bin/conf/zoo.cfg

vi apps/apache-zookeeper-3.8.0-bin/conf/zoo.cfg
```

配置文件说明：

```
# The number of milliseconds of each tick
# 通信心跳时间，Zookeeper服务器与客户端心跳时间，单位毫秒。
tickTime=2000

# The number of ticks that the initial 
# synchronization phase can take
# LF初始通信时限
# Leader和Follower初始连接时能容忍的最多心跳数，单位次（即tickTime的数量）
initLimit=10

# The number of ticks that can pass between 
# sending a request and getting an acknowledgement
# LF同步通信时限
# Leader和Follower连接之后，通信时能容忍的最多心跳数，单位次
# 时间如果超过syncLimit * tickTime，Leader认为Follwer挂掉，从服务器列表中删除Follwer
syncLimit=5

# the directory where the snapshot is stored.
# do not use /tmp for storage, /tmp here is just 
# example sakes.
# Zookeeper数据存放目录
dataDir=/home/wux_labs/data/zookeeper

# the port at which the clients will connect
# 客户端连接端口，通常不做修改
clientPort=2181
```

主要配置内容为：

```
# Zookeeper数据存放目录
dataDir=/home/wux_labs/data/zookeeper
# 客户端连接端口
clientPort=2181
```

创建数据存放目录。

```
mkdir -p /home/wux_labs/data/zookeeper
```

# 相关命令

如果没配置环境变量，则需要切换到安装目录下执行相关命令，或者指定命令的绝对路径。

```
cd apps/apache-zookeeper-3.8.0-bin
```

## 启动Zookeeper

```
bin/zkServer.sh start
```

![image-20230117150155819](images/image-20230117150155819.png)

QuorumPeerMain 就是 Zookeeper 服务端的进程。

## 查看状态

```
bin/zkServer.sh status
```

![image-20230117150349224](images/image-20230117150349224.png)

Mode: standalone 表示本地模式。

## 停止Zookeeper

```
bin/zkServer.sh stop
```

