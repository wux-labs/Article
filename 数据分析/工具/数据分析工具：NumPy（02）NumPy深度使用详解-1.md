# NumPy深度使用详解-1

NumPy是Python的一个常用科学计算库，它是Numerical Python的缩写。它的核心是一个多维数组对象（ndarray），这个对象是一个快速而灵活的容器，可以用于大量数据集和矩阵计算。

## 认识ndarray

ndarray是NumPy中最重要的数据结构，它是多维数组（n-dimensional array）的缩写。在numpy中，所有的数据都以ndarray的形式存储和处理。

ndarray有以下几个重要的属性：

* shape：表示数组的形状，即每个维度的大小。例如，一个形状为(3, 4)的ndarray表示一个3行4列的矩阵。

* dtype：表示数组的数据类型。例如，一个dtype为int32的ndarray表示每个元素都是32位整数。

* ndim：表示数组的维度，即数组的秩。例如，一个维度为2的ndarray表示一个二维数组。

**ndarray中的每个元素都必须是相同的数据类型**，并且在内存中是连续存储的，这使得NumPy能够非常高效地处理大规模的数据。

## 数组的创建

### 使用array函数创建数组

可以使用NumPy中的array函数来创建数组。

```python
import numpy as np

# 创建一维数组
a = np.array([1, 2, 3])
print(a.ndim, a.shape, a.dtype)
print(a)

# 创建二维数组
b = np.array([[1, 2, 3], [4, 5, 6]])
print(b.ndim, b.shape, b.dtype)
print(b)

# 创建不同数据类型的数组
a1 = np.array([-1, 0, 1], dtype=int)
a2 = np.array([-1, 0, 1], dtype=float)
a3 = np.array([-1, 0, 1], dtype=bool)
print(a1.dtype, a2.dtype, a3.dtype)
print(a1)
print(a2)
print(a3)
```

输出内容：

```
1 (3,) int32
[1 2 3]
2 (2, 3) int32
[[1 2 3]
 [4 5 6]]
int32 float64 bool
[-1  0  1]
[-1.  0.  1.]
[ True False  True]
```

![image-20230313222020603](images/image-20230313222020603.png)

### 生成有初始占位符的数组

可以使用`zeros`、`ones`、`empty`等函数来创建特定形状的数组。

* np.zeros(shape, dtype=float, order='C')，用于生成元素全为0的数组。
* np.ones(shape, dtype=float, order='C')，用于生成元素全为1的数组。
* np.empty(shape, dtype=float, order='C')，用于生成元素为随机数的数组。

```python
# 生成元素全为0的数组
a1 = np.zeros((2,3), dtype=float)
a2 = np.zeros((2,3), dtype=int)
a3 = np.zeros((2,3), dtype=bool)
print(a1)
print(a2)
print(a3)

# 生成元素全为1的数组
b1 = np.ones((2,4), dtype=float)
b2 = np.ones((2,4), dtype=int)
b3 = np.ones((2,4), dtype=bool)
print(b1)
print(b2)
print(b3)

# 生成元素为随机数的数组
c1 = np.empty((2,5), dtype=float)
c2 = np.empty((2,5), dtype=int)
c3 = np.empty((2,5), dtype=bool)
print(c1)
print(c2)
print(c3)
```

输出内容：

```
[[0. 0. 0.]
 [0. 0. 0.]]
[[0 0 0]
 [0 0 0]]
[[False False False]
 [False False False]]
[[1. 1. 1. 1.]
 [1. 1. 1. 1.]]
[[1 1 1 1]
 [1 1 1 1]]
[[ True  True  True  True]
 [ True  True  True  True]]
[[1.93101617e-312 1.44295714e-312 1.59149684e-312 6.57818695e-313
  1.90979621e-312]
 [8.48798317e-313 8.48798319e-314 1.06099790e-313 1.69759664e-313
  4.88059032e-313]]
[[93 75 23 23 93]
 [ 8 40 83 93 59]]
[[ True  True  True  True  True]
 [ True  True  True  True  True]]
```

![image-20230313224630615](images/image-20230313224630615.png)

### 生成固定范围的数组

NumPy中提供了一些方法，可以用来生成固定范围的数组：

* numpy.arange()，生成一个可指定起始值（默认为0）、终止值（不包含）、步长的数组。
* numpy.linspace()，生成一个可指定起始值、终止值、样本数的一维等差数列数组。
* numpy.logspace()，生成一个可指定起始值、终止值、样本数的一维对数数列数组。

```python
# 生成起始值1、终止值100、步长10的数组
a = np.arange(1, 100, 10)
print(a)

# 生成起始值1、终止值100、样本数10个的数组
b = np.linspace(1, 100, 10)
print(b)

# 生成起始值1、终止值2、以10为对数底数、样本数9个的数组
c = np.logspace(1.0, 2.0, num=9)
print(c)
```

输出内容：

```
[ 1 11 21 31 41 51 61 71 81 91]
[  1.  12.  23.  34.  45.  56.  67.  78.  89. 100.]
[ 10.          13.33521432  17.7827941   23.71373706  31.6227766
  42.16965034  56.23413252  74.98942093 100.        ]
```

![image-20230313225119875](images/image-20230313225119875.png)

### 生成随机数数组

NumPy中的random模块提供了各种随机数生成函数，例如rand()、randn()、randint()等，可以生成各种不同类型的随机数，这些随机数在模拟、实验和数据分析等任务中都非常有用。

* np.random.rand(d0, d1, ..., dn)：生成[0,1)之间均匀分布的随机数，参数为生成的数组的维度。

* np.random.random_sample(size=None)：与rand()函数相同，生成[0,1)之间均匀分布的随机数。
* np.random.random(size=None)：与random_sample()函数相同，生成在[0, 1)范围内的均匀分布随机数。
* np.random.randn(d0, d1, ..., dn)：生成标准正态分布（均值为0，标准差为1）的随机数，参数为生成的数组的维度。

```python
# 生成[0,1)之间均匀分布的随机数
a1 = np.random.rand(20)
a2 = np.random.rand(2, 20)
print(a1)
print(a2)

# 生成[0,1)之间均匀分布的随机数
b = np.random.random_sample(20)
print(b)

# 生成[0,1)之间均匀分布的随机数
c = np.random.random(20)
print(c)

# 生成标准正态分布（均值为0，标准差为1）的随机数
d = np.random.randn(20)
print(d)
```

![image-20230313225956290](images/image-20230313225956290.png)

```python
import matplotlib.pyplot as plt

# 数据可视化
figure, ax = plt.subplots(2, 2)

plt.subplot(2, 2, 1)
plt.hist(np.random.rand(2000))
plt.subplot(2, 2, 2)
plt.hist(np.random.random_sample(2000))
plt.subplot(2, 2, 3)
plt.hist(np.random.random(2000))
plt.subplot(2, 2, 4)
plt.hist(np.random.randn(2000))

plt.show()
```

![image-20230313230153574](images/image-20230313230153574.png)

* np.random.randint(low, high=None, size=None, dtype='l')：生成low和high之间的整数，包括low，但不包括high。若high参数没有给出，则生成[0, low)之间的整数。size参数为生成的数组的维度，dtype为所生成整数的数据类型。

* np.random.choice(a, size=None, replace=True, p=None)：从给定的一维数组a中生成随机样本，size参数为生成的数组的维度，replace参数指定是否可以重复采样，p参数为给定的一维数组a中每个元素被选择的概率。
* np.random.shuffle(x)：将给定的序列随机打乱。

```python
# 生成low和high之间的整数，包括low，但不包括high
a1 = np.random.randint(1, 100)
a2 = np.random.randint(1, 100, 20)
print(a1)
print(a2)

# 从给定的一维数组a中生成随机样本
b1 = np.random.choice(a2)
b2 = np.random.choice(a2, 10)
print(b1)
print(b2)

# 将给定的序列随机打乱
np.random.shuffle(a2)
print(a2)
```

![image-20230313231743948](images/image-20230313231743948.png)

## 数据类型转换

NumPy提供了各种数据类型转换函数，可以将数组从一种数据类型转换成另一种数据类型，例如astype()函数可以将数组的数据类型转换成指定的数据类型，这些函数在数据预处理和清洗中经常使用。

```python
# 创建一个浮点数类型的数组
a = np.array([-1, 0, 1], dtype=float)
print(a)

# 将浮点数转换成整数
b = a.astype(int)
print(b)

# 将整数转换成布尔值
c = a.astype(bool)
print(c)
```

![image-20230314000433256](images/image-20230314000433256.png)

## 数组的切片

切片是指对数组进行子集的选择，可以对多维数组进行操作。

当我们使用NumPy处理数据时，切片操作是非常常见的。下面是一些常见的NumPy数组切片操作的案例：

### 常规切片方法

* 选择一个二维数组的第一行：

```python
arr = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
first_row = arr[0,:]
print(first_row) # [1, 2, 3]
```

![image-20230313232740315](images/image-20230313232740315.png)

* 选择一个二维数组的第一列：

```python
arr = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
first_column = arr[:,0]
print(first_column) # [1, 4, 7]
```

![image-20230313232812550](images/image-20230313232812550.png)

* 选择一个二维数组的前两行：

```python
arr = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
first_two_rows = arr[:2,:]
print(first_two_rows) # [[1, 2, 3], [4, 5, 6]]
```

![image-20230313232845434](images/image-20230313232845434.png)

* 选择一个二维数组的后两列：

```python
arr = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
last_two_columns = arr[:,-2:]
print(last_two_columns) # [[2, 3], [5, 6], [8, 9]]
```

![image-20230313232932486](images/image-20230313232932486.png)

* 选择一个三维数组的第一个二维数组的前两行：

```python
arr = np.array([[[1, 2, 3], [4, 5, 6]], [[7, 8, 9], [10, 11, 12]]])
first_two_rows = arr[0,:2,:]
print(first_two_rows) # [[1, 2, 3], [4, 5, 6]]
```

![image-20230313233016111](images/image-20230313233016111.png)

### 高级切片方法

* 使用步长对数组进行切片：

```python
arr1 = np.array([1, 2, 3, 4, 5, 6])
sliced_arr1 = arr1[::2] #从0开始步长为2，选出 1 3 5 
print(sliced_arr1) # [1, 3, 5]

arr2 = np.array([[1, 2, 3], [4, 5, 6],[7, 8, 9], [10, 11, 12]])
sliced_arr2 = arr2[::2] #从0开始步长为2
print(sliced_arr2)
```

![image-20230313234251872](images/image-20230313234251872.png)

* 选择一个数组中满足条件的元素：

```python
arr1 = np.array([1, 2, 3, 4, 5, 6])
sliced_arr1 = arr1[arr1 > 3]
print(sliced_arr1) # [4, 5, 6]

arr2 = np.array([[1, 2, 3], [4, 5, 6],[7, 8, 9], [10, 11, 12]])
sliced_arr2 = arr2[arr2 > 3]
print(sliced_arr2)
```

![image-20230313234703209](images/image-20230313234703209.png)

* 使用多个条件对数组进行切片：

```python
arr = np.array([1, 2, 3, 4, 5, 6])
condition1 = arr > 2
condition2 = arr < 5
condition = np.logical_and(condition1, condition2)
sliced_arr = arr[condition]
print(sliced_arr) # [3, 4]
```

![image-20230313235319109](images/image-20230313235319109.png)

* 使用花式索引对数组进行切片：

```python
arr = np.array([1, 2, 3, 4, 5, 6])
index = [0, 3, 5]
sliced_arr = arr[index]
print(sliced_arr) # [1, 4, 6]
```

![image-20230313234745605](images/image-20230313234745605.png)

* 使用布尔掩码选择数组中满足条件的元素：

```python
arr = np.array([1, 2, 3, 4, 5, 6])
mask = np.array([True, False, True, False, True, False])
sliced_arr = arr[mask]
print(sliced_arr) # [1, 3, 5]
```

![image-20230313234817239](images/image-20230313234817239.png)

* 选择一个二维数组的斜对角线元素：

```python
arr = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
diagonal = arr.diagonal()
print(diagonal) # [1, 5, 9]
```

![image-20230313234850245](images/image-20230313234850245.png)

* 选择一个二维数组的副对角线元素：

```python
arr = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
diagonal = np.fliplr(arr).diagonal()
print(diagonal) # [3, 5, 7]
```

![image-20230313235042833](images/image-20230313235042833.png)

* 对数组进行切片并修改切片后的值：

```python
arr = np.array([1, 2, 3, 4, 5, 6])
sliced_arr = arr[2:5]
sliced_arr[:] = 0
print(sliced_arr) # [0, 0, 0]
print(arr) # [1, 2, 0, 0, 0, 6]
```

![image-20230313235441123](images/image-20230313235441123.png)

* 使用where()函数对数组进行切片：

```python
arr = np.array([1, 2, 3, 4, 5, 6])
sliced_arr = np.where(arr > 3, arr, 0)
print(sliced_arr) # [0, 0, 0, 4, 5, 6]
```

![image-20230313235546153](images/image-20230313235546153.png)

## 数组操作

NumPy提供了许多用于操作数组的函数，例如：reshape、concatenate、split、flatten等。这些函数使得数组的操作变得更加容易和灵活。

### 调整形状

reshape函数：将一个数组重新调整为指定的形状。例如：

```python
# 创建一个二维数组
a = np.array([[1, 2, 3], [4, 5, 6]])
print(a)

# 使用reshape函数将其转换为一个三行两列的数组
b = a.reshape(3, 2)
print(b)
```

输出内容：

```
[[1 2 3]
 [4 5 6]]
[[1 2]
 [3 4]
 [5 6]]
```

![image-20230314092307525](images/image-20230314092307525.png)

### 连接数组

concatenate函数：将两个或多个数组沿指定轴连接起来。例如：

```python
# 创建两个一维数组
a1 = np.array([1, 2, 3])
a2 = np.array([4, 5, 6])
print(a1)
print(a2)

# 使用concatenate函数将它们连接起来
b = np.concatenate([a1, a2])
print(b)

# 创建两个二维数组
c1 = np.array([[1, 2, 3], [4, 5, 6]])
c2 = np.array([[7, 8, 9], [10, 11, 12]])
print(c1)
print(c2)

# 使用concatenate函数将它们连接起来
d = np.concatenate([c1, c2])
print(d)
```

输出内容：

```
[1 2 3]
[4 5 6]
[1 2 3 4 5 6]
[[1 2 3]
 [4 5 6]]
[[ 7  8  9]
 [10 11 12]]
[[ 1  2  3]
 [ 4  5  6]
 [ 7  8  9]
 [10 11 12]]
```

![image-20230314092934197](images/image-20230314092934197.png)

### 分割数组

split函数：将一个数组沿指定轴分割成多个子数组。例如：

```python
# 创建一个一维数组
a1 = np.array([1, 2, 3, 4, 5, 6])
# 使用split函数将其分割成三个子数组
b1 = np.split(a1, 3)
print(b1)

# 创建一个二维数组
a2 = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12]])
# 使用split函数将其分割成三个子数组
b2 = np.split(a2, 2)
print(b2)
```

输出内容：

```
[array([1, 2]), array([3, 4]), array([5, 6])]
[array([[1, 2, 3],
       [4, 5, 6]]), array([[ 7,  8,  9],
       [10, 11, 12]])]
```

![image-20230314093627890](images/image-20230314093627890.png)

### 数组展平

flatten函数：将一个多维数组展平成一维数组。例如：

```
# 创建一个二维数组
a = np.array([[1, 2, 3], [4, 5, 6]])
print(a)
# 使用flatten函数将其展平成一维数组
b = a.flatten()
print(b)

# 创建一个三维数组
c = np.array([[[1, 2, 3], [4, 5, 6]], [[11, 12, 13], [14, 15, 16]]])
print(c)
# 使用flatten函数将其展平成一维数组
d = c.flatten()
print(d)
```

输出内容：

```
[[1 2 3]
 [4 5 6]]
[1 2 3 4 5 6]
[[[ 1  2  3]
  [ 4  5  6]]

 [[11 12 13]
  [14 15 16]]]
[ 1  2  3  4  5  6 11 12 13 14 15 16]
```

![image-20230314093907775](images/image-20230314093907775.png)

### 维度转置

transpose函数：将数组的维度进行转置操作。例如：

```python
# 创建一个二维数组
a = np.array([[1, 2, 3], [4, 5, 6]])
print(a)
# 使用transpose函数将其转置
b = np.transpose(a)
print(b)

# 创建一个三维数组
c = np.array([[[1, 2, 3], [4, 5, 6]], [[11, 12, 13], [14, 15, 16]]])
print(c)
# 使用transpose函数将其转置
d = np.transpose(c)
print(d)
```

输出内容：

```
[[1 2 3]
 [4 5 6]]
[[1 4]
 [2 5]
 [3 6]]
[[[ 1  2  3]
  [ 4  5  6]]

 [[11 12 13]
  [14 15 16]]]
[[[ 1 11]
  [ 4 14]]

 [[ 2 12]
  [ 5 15]]

 [[ 3 13]
  [ 6 16]]]
```

![image-20230314094134028](images/image-20230314094134028.png)

### 最大值的索引

argmax函数：返回数组中最大值的索引。例如：

```python
# 创建一个一维数组
a = np.array([1, 3, 2, 4, 5])
print(a)
# 使用argmax函数找到最大值的索引
b = np.argmax(a)
print(b)

# 创建一个二维数组
c = np.array([[1, 3, 2, 4, 5], [6, 7, 10, 9, 8]])
print(c)
# 使用argmax函数找到最大值的索引
d = np.argmax(c)
print(d)
```

输出内容：

```
[1 3 2 4 5]
4
[[ 1  3  2  4  5]
 [ 6  7 10  9  8]]
7
```

![image-20230314100219305](images/image-20230314100219305.png)

### 最小值的索引

argmin函数：返回数组中最小值的索引。例如：

```python
# 创建一个一维数组
a = np.array([11, 3, 2, 4, 5])
print(a)
# 使用argmin函数找到最小值的索引
b = np.argmin(a)
print(b)

# 创建一个二维数组
c = np.array([[11, 3, 12, 4, 5], [6, 7, 10, 9, 8]])
print(c)
# 使用argmin函数找到最小值的索引
d = np.argmin(c)
print(d)
```

输出内容：

```
[11  3  2  4  5]
2
[[11  3 12  4  5]
 [ 6  7 10  9  8]]
1
```

![image-20230314101833147](images/image-20230314101833147.png)

## 通用函数

NumPy中的ufunc（通用函数）可以对数组进行逐元素操作，例如exp()、log()、sin()、cos()等，这些函数在数学计算和科学计算中都经常使用。

### 指数值

np.exp()：返回一个数组中所有元素的指数值。

```python
# 创建一个一维数组
a = np.array([1, 2, 3])
print(a)
# 求所有元素的指数值
b = np.exp(a)
print(b)

# 创建一个二维数组
c = np.array([[1, 2, 3], [4, 5, 6]])
print(c)
# 求所有元素的指数值
d = np.exp(c)
print(d)
```

输出内容：

```
[1 2 3]
[ 2.71828183  7.3890561  20.08553692]
[[1 2 3]
 [4 5 6]]
[[  2.71828183   7.3890561   20.08553692]
 [ 54.59815003 148.4131591  403.42879349]]
```

![image-20230314103523752](images/image-20230314103523752.png)

### 对数值

np.log()：返回一个数组中所有元素的自然对数值。

```python
# 创建一个一维数组
a = np.array([1, 2, 3])
print(a)
# 求所有元素的对数值
b = np.log(a)
print(b)

# 创建一个二维数组
c = np.array([[1, 2, 3], [4, 5, 6]])
print(c)
# 求所有元素的对数值
d = np.log(c)
print(d)
```

输出内容：

```
[1 2 3]
[0.         0.69314718 1.09861229]
[[1 2 3]
 [4 5 6]]
[[0.         0.69314718 1.09861229]
 [1.38629436 1.60943791 1.79175947]]
```

![image-20230314103623777](images/image-20230314103623777.png)

### 正弦值

np.sin()：返回一个数组中所有元素的正弦值。

```python
a = np.array(np.linspace(-np.pi,np.pi,60,endpoint=True))
print(a)
b = np.sin(a)
print(b)

plt.plot(b)
plt.show()
```

![image-20230314105055785](images/image-20230314105055785.png)

### 余弦值

np.cos()：返回一个数组中所有元素的余弦值。

```python
a = np.array(np.linspace(-np.pi,np.pi,60,endpoint=True))
print(a)
b = np.cos(a)
print(b)

plt.plot(b)
plt.show()
```

![image-20230314105329207](images/image-20230314105329207.png)

### 平方根

np.sqrt()：返回一个数组中所有元素的平方根。

```python
a = np.array([1, 4, 9])
print(a)
b = np.sqrt(a)
print(b)

c = np.array([[1, 4, 9], [9, 16, 25]])
print(c)
d = np.sqrt(c)
print(d)
```

输出内容：

```
[1 4 9]
[1. 2. 3.]
[[ 1  4  9]
 [ 9 16 25]]
[[1. 2. 3.]
 [3. 4. 5.]]
```

![image-20230314110130215](images/image-20230314110130215.png)

### 幂运算

np.power()：返回一个数组中所有元素的幂。

```python
a = np.array([1, 2, 3])
print(a)
b = np.power(a, 3)
print(b)

c = np.array([[1, 2, 3], [4, 5, 6]])
print(c)
d = np.power(c, 2)
print(d)
```

输出内容：

```
[1 2 3]
[ 1  8 27]
[[1 2 3]
 [4 5 6]]
[[ 1  4  9]
 [16 25 36]]
```

![image-20230314111819461](images/image-20230314111819461.png)

### 绝对值

np.absolute()：返回一个数组中所有元素的绝对值。

```python
a = np.array([-1, -2, 3])
print(a)
b = np.absolute(a)
print(b)

c = np.array([[-1, -2, 3], [-3, 4, -5]])
print(c)
d = np.absolute(c)
print(d)
```

输出内容：

```
[-1 -2  3]
[1 2 3]
[[-1 -2  3]
 [-3  4 -5]]
[[1 2 3]
 [3 4 5]]
```

![image-20230314111911958](images/image-20230314111911958.png)

### 上限值

np.ceil()：返回一个数组中所有元素的上限值。

```python
a = np.array([1.1, 2.3, 3.5])
print(a)
b = np.ceil(a)
print(b)

c = np.array([[1.1, 2.3, 3.5], [3.45, 5.78, -5.9]])
print(c)
d = np.ceil(c)
print(d)
```

输出内容：

```
[1.1 2.3 3.5]
[2. 3. 4.]
[[ 1.1   2.3   3.5 ]
 [ 3.45  5.78 -5.9 ]]
[[ 2.  3.  4.]
 [ 4.  6. -5.]]
```

![image-20230314112007417](images/image-20230314112007417.png)

### 下限值

np.floor()：返回一个数组中所有元素的下限值。

```python
a = np.array([1.1, 2.3, 3.5])
print(a)
b = np.floor(a)
print(b)

c = np.array([[1.1, 2.3, 3.5], [3.45, 5.78, -5.9]])
print(c)
d = np.floor(c)
print(d)
```

输出内容：

```
[1.1 2.3 3.5]
[1. 2. 3.]
[[ 1.1   2.3   3.5 ]
 [ 3.45  5.78 -5.9 ]]
[[ 1.  2.  3.]
 [ 3.  5. -6.]]
```

![image-20230314112104809](images/image-20230314112104809.png)

# 总结

NumPy提供了丰富的数组操作、数学函数、线性代数、随机数生成等功能，为科学计算和数据分析提供了强大的支持。以上列出的仅仅是一些常见的操作，还有更多更强大的功能我们下次继续探索。
