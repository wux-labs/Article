# 安装Python环境

所谓“工欲善其事，必先利其器”。在学习Python之前需要先搭建Python的运行环境。由于Python是跨平台的，支持在各种操作系统平台上安装运行，需要确定好自己需要使用的操作系统。

对于初学者安装Python，可以直接通过Anaconda进行安装。Anaconda是一个集成的Python运行环境，除了包含Python本身的运行环境外，还集成了很多第三方模块，对于初学者来说这是非常省事的。Anaconda的官方网站为[Anaconda | The World's Most Popular Data Science Platform](https://www.anaconda.com/)。

## 在Windows上安装Python

Anaconda的安装比较简单，首先进入Anaconda的下载页面，[Anaconda | Anaconda Distribution](https://www.anaconda.com/products/distribution)，由于Python 2.x已经停止更新，所以目前只能下载Python 3.x版本的，最新的Anaconda是Python 3.9版本的。选择Windows安装包，点击下载即可。

![image-20230209232659898](images/image-20230209232659898.png)

下载完成后：

1、直接双击运行，启动安装向导程序。

![image-20230209233308565](images/image-20230209233308565.png)

2、点击Next按钮进入下一步。

![image-20230209233515380](images/image-20230209233515380.png)

3、点击I Agree按钮，同意用户协议，并进入下一步。

![image-20230209233617742](images/image-20230209233617742.png)

4、在安装方式这里选择Just Me，只在当前用户安装，点击Next按钮进入下一步。

![image-20230209233737361](images/image-20230209233737361.png)

5、选择目标安装路径，这是Anaconda最终安装的位置，点击Next按钮进入下一步。

![image-20230209233945989](images/image-20230209233945989.png)

6、选择将Anaconda3注册成默认的Python 3.9运行环境，点击Install按钮开始安装。

![image-20230209234048225](images/image-20230209234048225.png)

7、Anaconda安装的过程中会显示安装进度，只需要等待其安装完成即可。

![image-20230210002109800](images/image-20230210002109800.png)

8、安装完成后，点击Next按钮进入下一步。

![image-20230210002149012](images/image-20230210002149012.png)

9、继续点击Next按钮。

![image-20230210002231794](images/image-20230210002231794.png)

10、在最后一个界面上点击Finish按钮完成安装。

### 验证Python运行环境

点击Windows的开始菜单。

![image-20230210002452635](images/image-20230210002452635.png)

可以看到新安装的Anaconda程序。

![image-20230210002701390](images/image-20230210002701390.png)

点击Anaconda Prompt(anaconda3)，打开一个命令行窗口。输入python命令可以进入Python的解释器环境。

![image-20230210002856881](images/image-20230210002856881.png)

在解释器环境中可以看到Python的版本是3.9.13，这说明Python运行环境安装成功。

输入一段Python代码，执行代码看看运行结果。

```
print("Hello World")
```

可以看到正常的输出。

![image-20230210003157931](images/image-20230210003157931.png)

## 在Linux上安装Python

在Linux上安装Python，推荐使用Ubuntu操作系统。首先进入Anaconda的下载页面，[Anaconda | Anaconda Distribution](https://www.anaconda.com/products/distribution)，找到Linux的安装包，点击下载后上传到Linux系统。

![image-20230209235810482](images/image-20230209235810482.png)

或者复制下载链接，直接在Linux系统上通过命令下载。

```
wget https://repo.anaconda.com/archive/Anaconda3-2022.10-Linux-x86_64.sh
```

![image-20230209235917046](images/image-20230209235917046.png)

下载完成后：

1、直接运行安装。

```
sh Anaconda3-2022.10-Linux-x86_64.sh
```

![image-20230210000145174](images/image-20230210000145174.png)

2、按照提示，按ENTER键，阅读用户协议。

![image-20230210000300799](images/image-20230210000300799.png)

3、按空格键继续阅读，或者按Q键退出阅读。

4、输入yes同意用户协议。

```
Do you accept the license terms? [yes|no]
[no] >>> yes
```

![image-20230210000635601](images/image-20230210000635601.png)

5、确定Anaconda的安装目录，直接按ENTER键采用默认目录。

```
Anaconda3 will now be installed into this location:
/home/wux_labs/anaconda3

  - Press ENTER to confirm the location
  - Press CTRL-C to abort the installation
  - Or specify a different location below

[/home/wux_labs/anaconda3] >>> 
```

![image-20230210000651952](images/image-20230210000651952.png)

6、输入yes，让Anaconda为我们初始化相关的环境变量等信息。

```
installation finished.
Do you wish the installer to initialize Anaconda3
by running conda init? [yes|no]
[no] >>> yes
```

![image-20230210000953800](images/image-20230210000953800.png)

7、初始完成后，我们会发现Anaconda为我们修改了.bashrc文件。

![image-20230210001142824](images/image-20230210001142824.png)

这个文件里面配置了Anaconda相关的一些环境变量。

![image-20230210001313112](images/image-20230210001313112.png)

重新登录一下系统，让这些环境变量生效，可以看到当前进入了base虚拟环境。

```
(base) wux_labs@wux-labs-vm:~$ 
```

### 验证Python运行环境

执行python命令，可以进入Python的解释器环境，可以看到当前的Python版本是3.9.13，这说明Python环境安装成功了。

![image-20230210001620017](images/image-20230210001620017.png)

编写Python代码，执行一下看看结果。

```
print("Hello World")
```

可以看到正常的输出。

![image-20230210002012731](images/image-20230210002012731.png)

# 总结

通过Anaconda安装Python的运行环境比较简单，只需要按照安装向导一步一步进行安装即可。

