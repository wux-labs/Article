# Python基本数据类型

Python是一种动态类型语言，它支持多种基本数据类型和复合数据类型，让开发人员能够更加方便地处理不同类型的数据。本文将介绍Python中的基本数据类型，包括整数、浮点数、布尔值、字符串等。

## 整数（int）

整数是Python中最基本的数据类型之一，它表示整数，例如 1、2、3、5等。Python的整数类型可以表示任意大小的整数，因此可以处理非常大的整数，而不需要担心溢出的问题。

在Python中，整数可以使用十进制、二进制、八进制、十六进制等方式表示，例如：

```python
# 十进制
a = 123

# 二进制
b = 0b1010

# 八进制
c = 0o123

# 十六进制
d = 0x123

# 很大的数
e = 999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999
```

可以使用 `type()` 函数来查看变量的类型，例如：

```python
print(type(a))
print(type(b))
print(type(c))
print(type(d))
print(type(e))
```

![image-20230302142804886](images/image-20230302142804886.png)

Python中的整数运算包括基本的四则运算（加、减、乘、除）以及其他的位运算、比较运算等。

### 基本的四则运算

基本的四则运算在Python中与数学中的运算规则一致，分别使用加（+）、减（-）、乘（*）、除（/）和地板除（floor division，除法结果向下取整，//）符号进行运算。

```python
a = 5
b = 3

print(a + b)   # 输出7
print(a - b)   # 输出3
print(a * b)   # 输出10
print(a / b)   # 输出1.6666666666666667
print(a // b)  # 输出1
```

![image-20230302143630751](images/image-20230302143630751.png)

> 需要注意的是，在Python 2中整数除法的结果会自动向下取整，而在Python 3中整数除法的结果为**浮点数**。如果想要在Python 3中使用向下取整的除法，可以使用地板除（//）符号。

**注意：**`//`得到的并不一定是整数类型的数，它与分母分子的数据类型有关系。

![image-20230302152939887](images/image-20230302152939887.png)

### 位运算

位运算是指对整数的二进制位进行操作的运算，包括按位与（&）、按位或（|）、按位异或（^）、左移（<<）和右移（>>）等运算符。

```python
a = 5  # 二进制表示为101
b = 3  # 二进制表示为011

print(a & b)  # 输出1，二进制表示为001
print(a | b)  # 输出7，二进制表示为111
print(a ^ b)  # 输出6，二进制表示为110
print(a << 1)  # 输出10，二进制表示为1010
print(a >> 1)  # 输出2，二进制表示为10
```

![image-20230302144708621](images/image-20230302144708621.png)

> 左移和右移运算符可以将整数的二进制位向左或向右移动一定的位数（n），向左移动等价于乘以2的n次方（2 ** n），向右移动等价于除以2的n次方。

###  比较运算

比较运算用于比较两个数的大小关系，包括等于（==）、不等于（!=）、大于（>）、小于（<）、大于等于（>=）和小于等于（<=）等运算符。

```python
a = 5
b = 3

print(a == b)  # 输出False
print(a != b)  # 输出True
print(a > b)  # 输出True
print(a < b)  # 输出False
print(a >= b)  # 输出True
print(a <= b)  # 输出False
```

![image-20230302151624303](images/image-20230302151624303.png)

### 运算优先级

在进行复杂的数值运算时，需要注意运算优先级。在Python中，运算符的优先级从高到低分别是：

- 幂运算 `**`
- 正负号 `+x, -x`
- 乘法、除法、取余运算 `*, /, %, //`
- 加法、减法运算 `+, -`

> 需要注意的是，如果需要改变运算的优先级，可以使用括号将运算表达式括起来。

## 浮点数（float）

浮点数是带小数的数字，在Python中用浮点数类型（float）表示，例如 3.14、-0.5 等。浮点型也可以使用科学计数法表示（2.5e2 = 2.5 x 102 = 250）。

在Python中，浮点数的精度是有限的，因此在进行数学计算时，可能会出现精度误差。例如：

```python
a = 0.1 + 0.2

print(a)  # 输出0.30000000000000004
```

![image-20230302152243257](images/image-20230302152243257.png)

可以使用 `round()` 函数来四舍五入浮点数，例如：

```python
a = 0.1 + 0.2
a = round(a, 2)

print(a)  #输出0.3
```

![image-20230302152513823](images/image-20230302152513823.png)

> 浮点数与整数一样，也支持四则运算、比较运算等，但不支持位运算。

## 布尔值（bool）

布尔值只有两个取值`True`和`False`，用于表示真或假。在Python中，可以将任何值转换为布尔值，使用 `bool()` 函数即可。例如：

```python
print(bool(0))      # 输出False
print(bool(1))      # 输出True
print(bool(''))     # 输出False
print(bool('abc'))  # 输出True
```

![image-20230302154535161](images/image-20230302154535161.png)

布尔类型在Python中广泛应用于条件判断和循环等控制语句中，例如 `if` 语句、`while` 循环和 `for` 循环等。

```python
a = True
b = False

if a and not b:
    print("a是True并且b是False")

for i in range(10):
    if i % 2 == 0:
        continue
    print(i)

while a or b:
    print("Looping...")
    a = a and b
    b = a and b
```

![image-20230302155011095](images/image-20230302155011095.png)

## 字符串（str）

字符串是一组字符的序列，用于表示文本。在Python中，字符串类型（str）可以使用单引号、双引号或三引号来定义，例如：'hello'、 "world" 等。

字符串是不可变的，因此不能修改字符串中的字符。

可以使用索引（下标）访问字符串中的字符，例如：

```python
s = 'hello'

print(s[0])   # 输出'h'
print(s[-1])  # 输出'o'
```

![image-20230302155238864](images/image-20230302155238864.png)

可以使用字符串的切片访问字符串中的字符，或者创建一个新的字符串，例如：

```python
s1 = 'hello'
s2 = s1[1:4]

print(s1[1:3])   # 输出'ell'
print(s2)        # 输出'ello'
```

![image-20230302155411444](images/image-20230302155411444.png)

可以使用加号（+）将两个字符串连接起来，使用乘号（*）可以将字符串重复多次，例如：

```python
s1 = 'hello'
s2 = 'world'

print(s1 + s2)  # 输出'helloworld'
```

![image-20230302155457494](images/image-20230302155457494.png)

除了常规的字符串操作之外，Python还提供了一些内置方法来处理字符串。例如，我们可以使用 `split()` 方法来将字符串分割成列表，使用 `strip()` 方法来去除字符串两端的空格，使用 `replace()` 方法来替换字符串中的某个字符或子串等等。

```python
s1 = "hello,world"
lst = s1.split(",")                 # 将字符串以逗号为分隔符，分割成列表
s2 = s1.strip()                     # 去除字符串两端的空格
s3 = s1.replace("world", "Python")  # 将字符串中的"world"替换为"Python"
print(s1)
print(lst)
print(s2)
print(s3)
```

![image-20230302155659552](images/image-20230302155659552.png)

# Python数据类型变换

在Python中，数据类型并不是一成不变的，数据类型可以根据实际情况做变换。

Python是一种动态语言，变量在使用之前不需要指定数据类型，而是在运行时根据赋值的值自动确定类型，因此Python的数据类型变换非常灵活。

## 隐式类型转换

在Python中，有些类型之间的运算是自动进行类型转换的，这种类型转换称为隐式类型转换。

例如，当整数和浮点数进行运算时，整数会自动转换为浮点数：

```python
a = 5     # a为整数
b = 3.14  # b为浮点数
c = a + b # a自动转换为浮点数，c为8.14
```

在隐式类型转换时，Python会自动选择合适的类型进行转换，但是这种转换不一定总是符合我们的期望，因此有时需要手动进行类型转换。

## 显式类型转换

Python中可以使用一些内置函数进行显式类型转换，常用的内置函数包括int()、float()、str()、bool()等。

```python
a = "5"       # a为字符串
b = int(a)    # a转换为整数，b为5
c = float(b)  # b转换为浮点数，c为5.0
d = bool(c)   # c转换为布尔类型，d为True
e = str(d)    # d转换为字符串，e为"True"
print(a, type(a))
print(b, type(b))
print(c, type(c))
print(d, type(d))
print(e, type(e))
```

![image-20230302160720193](images/image-20230302160720193.png)

> 在进行类型转换时，有时会出现类型不匹配的错误，例如在字符串中包含非数字字符时使用int()函数进行转换会出错。因此在进行类型转换时需要谨慎，并确保数据类型的一致性。

##  强制类型转换

有时候，我们需要将一个对象强制转换为另一个类型。Python中可以使用各个数据类型所对应的构造函数进行强制类型转换。例如，可以使用int()构造函数将一个浮点数或字符串转换为整数。

```python
a = 5.8     # a为浮点数
b = int(a)  # a强制转换为整数，b为5

print(a, type(a))
print(b, type(b))
```

![image-20230302162048423](images/image-20230302162048423.png)

> 强制类型转换可能会丢失数据，例如将一个浮点数强制转换为整数时，会将小数部分直接截断，导致数据的精度损失。

## 赋不同类型的值

在Python中，对于同一个变量，还可以通过赋值的方式改变变量的类型。

```python
a = 5
print("现在我是整数：", a, type(a))
a = 5.6
print("现在我是浮点数：", a, type(a))
a = True
print("现在我是布尔值：", a, type(a))
a = "变来变去"
print("现在我是字符串：", a, type(a))
```

![image-20230302162519157](images/image-20230302162519157.png)

# 写在后面

Python是一门强大的编程语言，拥有丰富的基本数据类型。在本篇文章中，我们介绍了Python的几种基本数据类型：整数、浮点数、布尔值和字符串，以及它们之间的相互转换。同时，我们还讨论了Python中的算术运算、比较运算和逻辑运算，以及它们的应用。
