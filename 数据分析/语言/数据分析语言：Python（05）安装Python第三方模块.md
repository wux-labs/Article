# 安装Python第三方模块

## 为什么需要安装第三方模块

Python具有很强的可扩展性，它不是把所有功能特性都集中到语言核心，而是提供了丰富的官方的、第三方的类库。在创建好一个Python的虚拟环境之后，默认安装的包非常少，不能满足开发中的需求，所以需要我们自己安装第三方模块。

* 在Windows下

![image-20230211001118718](images/image-20230211001118718.png)

* 在Linux下

![image-20230211003953833](images/image-20230211003953833.png)

## Python包管理器介绍

很多系统和语言都提供了包管理器。

### pip

Python 常用的包管理器是 pip。pip的使用方法为：

```
Usage:   
  pip <command> [options]

Commands:
  install                     Install packages.
  download                    Download packages.
  uninstall                   Uninstall packages.
  freeze                      Output installed packages in requirements format.
  inspect                     Inspect the python environment.
  list                        List installed packages.
  show                        Show information about installed packages.
  check                       Verify installed packages have compatible dependencies.
  config                      Manage local and global configuration.
  search                      Search PyPI for packages.
  cache                       Inspect and manage pip's wheel cache.
  index                       Inspect information available from package indexes.
  wheel                       Build wheels from your requirements.
  hash                        Compute hashes of package archives.
  completion                  A helper command used for command completion.
  debug                       Show information useful for debugging.
  help                        Show help for commands.
```

常用的命令有：

* install，用于安装Python模块
* download，用于下载Python模块
* uninstall，用于卸载已安装的模块
* freeze，用于输出当前环境中已安装的模块，通常用于生成依赖文件requirements.txt
* list，列出当前环境中已安装的模块
* search，用于从PyPI中查找第三方模块

有两种方法来运行pip进行Python模块的安装，这两种方法的效果是一样的。

#### pip install

使用pip进行Python模块安装的第一种方式是直接使用命令`pip install`，这个是安装Python模块最方便的方式。当然，前提是pip命令已经添加到系统的环境变量PATH中了。pip install命令的语法为：

```
Usage:   
  pip install [options] <requirement specifier> [package-index-options] ...
  pip install [options] -r <requirements file> [package-index-options] ...
  pip install [options] [-e] <vcs project url> ...
  pip install [options] [-e] <local project path> ...
  pip install [options] <archive url/path> ...

Description:
  Install packages from:
  
  - PyPI (and other indexes) using requirement specifiers.
  - VCS project urls.
  - Local project directories.
  - Local or remote source archives.
```

#### python -m pip install

使用pip进行Python模块安装的第二种方式是使用命令`python -m pip install`，其中，`-m`指定要运行的模块，该命令是直接使用python命令，将pip模块当成脚本运行。python -m pip install命令的语法为：

```
Usage:   
  /home/wux_labs/anaconda3/envs/PythonBasic/bin/python -m pip install [options] <requirement specifier> [package-index-options] ...
  /home/wux_labs/anaconda3/envs/PythonBasic/bin/python -m pip install [options] -r <requirements file> [package-index-options] ...
  /home/wux_labs/anaconda3/envs/PythonBasic/bin/python -m pip install [options] [-e] <vcs project url> ...
  /home/wux_labs/anaconda3/envs/PythonBasic/bin/python -m pip install [options] [-e] <local project path> ...
  /home/wux_labs/anaconda3/envs/PythonBasic/bin/python -m pip install [options] <archive url/path> ...

Description:
  Install packages from:
  
  - PyPI (and other indexes) using requirement specifiers.
  - VCS project urls.
  - Local project directories.
  - Local or remote source archives.
```

### conda

Anaconda提供的conda是一个用来管理和部署应用、环境和包的工具，自然conda工具也包含第三方模块的安装功能。

#### conda install

conda install命令就是用来安装Python模块的，其语法如下：

```
usage: conda install [-h] [--revision REVISION] [-n ENVIRONMENT | -p PATH] [-c CHANNEL] [--use-local] [--override-channels]
                     [--repodata-fn REPODATA_FNS] [--strict-channel-priority] [--no-channel-priority] [--no-deps | --only-deps] [--no-pin] [--copy]
                     [-C] [-k] [--offline] [-d] [--json] [-q] [-v] [-y] [--download-only] [--show-channel-urls] [--file FILE]
                     [--experimental-solver {classic,libmamba,libmamba-draft}] [--force-reinstall]
                     [--freeze-installed | --update-deps | -S | --update-all | --update-specs] [-m] [--clobber] [--dev]
                     [package_spec ...]

Installs a list of packages into a specified conda environment.
```

## 在Windows环境中安装Python模块

打开Anaconda Prompt窗口，切换到需要安装Python模块的虚拟环境。

```
conda activate PythonBasic
```

在数据分析中，经常会用到Python数据分析三剑客：numpy、pandas、matplotlib，下面分别用三种方式来进行安装。

### 安装numpy

使用命令：

```
pip install numpy
```

![image-20230215151815115](images/image-20230215151815115.png)

安装完成后，可以看到虚拟环境中的Python模块明显比最初的时候多很多。

编写一段代码验证一下：

```python
(PythonBasic) C:\Users\wux_labs>python
Python 3.9.16 (main, Jan 11 2023, 16:16:36) [MSC v.1916 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> import numpy as np
>>>
>>> arr = np.arange(12)
>>> print(arr)
[ 0  1  2  3  4  5  6  7  8  9 10 11]
>>> arr2 = arr.reshape(3,4)
>>> print(arr2)
[[ 0  1  2  3]
 [ 4  5  6  7]
 [ 8  9 10 11]]
>>>
```

![image-20230215152354955](images/image-20230215152354955.png)

### 安装pandas

使用命令：

```
python -m pip install pandas
```

![image-20230215152547608](images/image-20230215152547608.png)

安装完成后，编写代码验证一下：

```python
(PythonBasic) C:\Users\wux_labs>python
Python 3.9.16 (main, Jan 11 2023, 16:16:36) [MSC v.1916 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> import pandas as pd
>>>
>>> ser1 = pd.Series([1.5, 2.5, 3, 4.5, 5.0, 6])
>>> print(ser1)
0    1.5
1    2.5
2    3.0
3    4.5
4    5.0
5    6.0
dtype: float64
>>>
```

![image-20230215152807830](images/image-20230215152807830.png)

### 安装matplotlib

使用命令：

```
conda install matplotlib
```

可以看到matplotlib安装的包更多，安装过程为：

```bash
(PythonBasic) C:\Users\wux_labs>conda install matplotlib
Collecting package metadata (current_repodata.json): done
Solving environment: done


==> WARNING: A newer version of conda exists. <==
  current version: 22.9.0
  latest version: 23.1.0

Please update conda by running

    $ conda update -n base -c defaults conda



## Package Plan ##

  environment location: C:\Users\wux_labs\anaconda3\envs\PythonBasic

  added / updated specs:
    - matplotlib


The following packages will be downloaded:

    package                    |            build
    ---------------------------|-----------------
    contourpy-1.0.5            |   py39h59b6b97_0         159 KB
    freetype-2.12.1            |       ha860e81_0         490 KB
    glib-2.69.1                |       h5dc1a3c_2         1.8 MB
    gst-plugins-base-1.18.5    |       h9e645db_0         1.7 MB
    gstreamer-1.18.5           |       hd78058f_0         1.7 MB
    kiwisolver-1.4.4           |   py39hd77b12b_0          60 KB
    libclang-12.0.0            |default_h627e005_2        17.8 MB
    libffi-3.4.2               |       hd77b12b_6         109 KB
    libogg-1.3.5               |       h2bbff1b_1          33 KB
    libtiff-4.5.0              |       h6c2663c_1         1.2 MB
    libvorbis-1.3.7            |       he774522_0         202 KB
    libwebp-1.2.4              |       h2bbff1b_0          67 KB
    libwebp-base-1.2.4         |       h2bbff1b_0         279 KB
    lz4-c-1.9.4                |       h2bbff1b_0         143 KB
    matplotlib-3.6.2           |   py39haa95532_0           9 KB
    matplotlib-base-3.6.2      |   py39h1094b8e_0         6.5 MB
    numpy-1.23.5               |   py39h3b20f71_0          11 KB
    numpy-base-1.23.5          |   py39h4da318b_0         6.0 MB
    openssl-1.1.1t             |       h2bbff1b_0         5.5 MB
    pcre-8.45                  |       hd77b12b_0         382 KB
    pillow-9.3.0               |   py39hd77b12b_2         992 KB
    ply-3.11                   |   py39haa95532_0          81 KB
    pyqt-5.15.7                |   py39hd77b12b_0         3.7 MB
    pyqt5-sip-12.11.0          |   py39hd77b12b_0          75 KB
    qt-main-5.15.2             |       he8e5bd7_7        50.0 MB
    qt-webengine-5.15.9        |       hb9a9bb5_5        48.9 MB
    qtwebkit-5.212             |       h3ad3cdb_4        10.3 MB
    sip-6.6.2                  |   py39hd77b12b_0         434 KB
    xz-5.2.10                  |       h8cc25b3_1         520 KB
    zlib-1.2.13                |       h8cc25b3_0         113 KB
    ------------------------------------------------------------
                                           Total:       158.9 MB

The following NEW packages will be INSTALLED:

  blas               pkgs/main/win-64::blas-1.0-mkl None
  brotli             pkgs/main/win-64::brotli-1.0.9-h2bbff1b_7 None
  brotli-bin         pkgs/main/win-64::brotli-bin-1.0.9-h2bbff1b_7 None
  contourpy          pkgs/main/win-64::contourpy-1.0.5-py39h59b6b97_0 None
  cycler             pkgs/main/noarch::cycler-0.11.0-pyhd3eb1b0_0 None
  fonttools          pkgs/main/noarch::fonttools-4.25.0-pyhd3eb1b0_0 None
  freetype           pkgs/main/win-64::freetype-2.12.1-ha860e81_0 None
  glib               pkgs/main/win-64::glib-2.69.1-h5dc1a3c_2 None
  gst-plugins-base   pkgs/main/win-64::gst-plugins-base-1.18.5-h9e645db_0 None
  gstreamer          pkgs/main/win-64::gstreamer-1.18.5-hd78058f_0 None
  icu                pkgs/main/win-64::icu-58.2-ha925a31_3 None
  intel-openmp       pkgs/main/win-64::intel-openmp-2021.4.0-haa95532_3556 None
  jpeg               pkgs/main/win-64::jpeg-9e-h2bbff1b_0 None
  kiwisolver         pkgs/main/win-64::kiwisolver-1.4.4-py39hd77b12b_0 None
  lerc               pkgs/main/win-64::lerc-3.0-hd77b12b_0 None
  libbrotlicommon    pkgs/main/win-64::libbrotlicommon-1.0.9-h2bbff1b_7 None
  libbrotlidec       pkgs/main/win-64::libbrotlidec-1.0.9-h2bbff1b_7 None
  libbrotlienc       pkgs/main/win-64::libbrotlienc-1.0.9-h2bbff1b_7 None
  libclang           pkgs/main/win-64::libclang-12.0.0-default_h627e005_2 None
  libdeflate         pkgs/main/win-64::libdeflate-1.8-h2bbff1b_5 None
  libffi             pkgs/main/win-64::libffi-3.4.2-hd77b12b_6 None
  libiconv           pkgs/main/win-64::libiconv-1.16-h2bbff1b_2 None
  libogg             pkgs/main/win-64::libogg-1.3.5-h2bbff1b_1 None
  libpng             pkgs/main/win-64::libpng-1.6.37-h2a8f88b_0 None
  libtiff            pkgs/main/win-64::libtiff-4.5.0-h6c2663c_1 None
  libvorbis          pkgs/main/win-64::libvorbis-1.3.7-he774522_0 None
  libwebp            pkgs/main/win-64::libwebp-1.2.4-h2bbff1b_0 None
  libwebp-base       pkgs/main/win-64::libwebp-base-1.2.4-h2bbff1b_0 None
  libxml2            pkgs/main/win-64::libxml2-2.9.14-h0ad7f3c_0 None
  libxslt            pkgs/main/win-64::libxslt-1.1.35-h2bbff1b_0 None
  lz4-c              pkgs/main/win-64::lz4-c-1.9.4-h2bbff1b_0 None
  matplotlib         pkgs/main/win-64::matplotlib-3.6.2-py39haa95532_0 None
  matplotlib-base    pkgs/main/win-64::matplotlib-base-3.6.2-py39h1094b8e_0 None
  mkl                pkgs/main/win-64::mkl-2021.4.0-haa95532_640 None
  mkl-service        pkgs/main/win-64::mkl-service-2.4.0-py39h2bbff1b_0 None
  mkl_fft            pkgs/main/win-64::mkl_fft-1.3.1-py39h277e83a_0 None
  mkl_random         pkgs/main/win-64::mkl_random-1.2.2-py39hf11a4ad_0 None
  munkres            pkgs/main/noarch::munkres-1.1.4-py_0 None
  numpy              pkgs/main/win-64::numpy-1.23.5-py39h3b20f71_0 None
  numpy-base         pkgs/main/win-64::numpy-base-1.23.5-py39h4da318b_0 None
  pcre               pkgs/main/win-64::pcre-8.45-hd77b12b_0 None
  pillow             pkgs/main/win-64::pillow-9.3.0-py39hd77b12b_2 None
  ply                pkgs/main/win-64::ply-3.11-py39haa95532_0 None
  pyparsing          pkgs/main/win-64::pyparsing-3.0.9-py39haa95532_0 None
  pyqt               pkgs/main/win-64::pyqt-5.15.7-py39hd77b12b_0 None
  pyqt5-sip          pkgs/main/win-64::pyqt5-sip-12.11.0-py39hd77b12b_0 None
  qt-main            pkgs/main/win-64::qt-main-5.15.2-he8e5bd7_7 None
  qt-webengine       pkgs/main/win-64::qt-webengine-5.15.9-hb9a9bb5_5 None
  qtwebkit           pkgs/main/win-64::qtwebkit-5.212-h3ad3cdb_4 None
  sip                pkgs/main/win-64::sip-6.6.2-py39hd77b12b_0 None
  tk                 pkgs/main/win-64::tk-8.6.12-h2bbff1b_0 None
  toml               pkgs/main/noarch::toml-0.10.2-pyhd3eb1b0_0 None
  xz                 pkgs/main/win-64::xz-5.2.10-h8cc25b3_1 None
  zlib               pkgs/main/win-64::zlib-1.2.13-h8cc25b3_0 None
  zstd               pkgs/main/win-64::zstd-1.5.2-h19a0ad4_0 None

The following packages will be UPDATED:

  openssl                                 1.1.1s-h2bbff1b_0 --> 1.1.1t-h2bbff1b_0 None


Proceed ([y]/n)? y


Downloading and Extracting Packages
numpy-1.23.5         | 11 KB     | ############################################################################ | 100%
libwebp-1.2.4        | 67 KB     | ############################################################################ | 100%
sip-6.6.2            | 434 KB    | ############################################################################ | 100%
ply-3.11             | 81 KB     | ############################################################################ | 100%
pillow-9.3.0         | 992 KB    | ############################################################################ | 100%
glib-2.69.1          | 1.8 MB    | ############################################################################ | 100%
matplotlib-3.6.2     | 9 KB      | ############################################################################ | 100%
contourpy-1.0.5      | 159 KB    | ############################################################################ | 100%
qt-webengine-5.15.9  | 48.9 MB   | ############################################################################ | 100%
kiwisolver-1.4.4     | 60 KB     | ############################################################################ | 100%
gst-plugins-base-1.1 | 1.7 MB    | ############################################################################ | 100%
qtwebkit-5.212       | 10.3 MB   | ############################################################################ | 100%
libclang-12.0.0      | 17.8 MB   | ############################################################################ | 100%
pcre-8.45            | 382 KB    | ############################################################################ | 100%
libogg-1.3.5         | 33 KB     | ############################################################################ | 100%
freetype-2.12.1      | 490 KB    | ############################################################################ | 100%
libffi-3.4.2         | 109 KB    | ############################################################################ | 100%
openssl-1.1.1t       | 5.5 MB    | ############################################################################ | 100%
qt-main-5.15.2       | 50.0 MB   | ############################################################################ | 100%
pyqt-5.15.7          | 3.7 MB    | ############################################################################ | 100%
libwebp-base-1.2.4   | 279 KB    | ############################################################################ | 100%
zlib-1.2.13          | 113 KB    | ############################################################################ | 100%
lz4-c-1.9.4          | 143 KB    | ############################################################################ | 100%
libvorbis-1.3.7      | 202 KB    | ############################################################################ | 100%
numpy-base-1.23.5    | 6.0 MB    | ############################################################################ | 100%
libtiff-4.5.0        | 1.2 MB    | ############################################################################ | 100%
matplotlib-base-3.6. | 6.5 MB    | ############################################################################ | 100%
xz-5.2.10            | 520 KB    | ############################################################################ | 100%
pyqt5-sip-12.11.0    | 75 KB     | ############################################################################ | 100%
gstreamer-1.18.5     | 1.7 MB    | ############################################################################ | 100%
Preparing transaction: done
Verifying transaction: done
Executing transaction: done
Retrieving notices: ...working... done

(PythonBasic) C:\Users\wux_labs>
```

![image-20230215153409026](images/image-20230215153409026.png)

安装完成后，编写一段代码验证一下：

```
(PythonBasic) C:\Users\wux_labs>python
Python 3.9.16 (main, Jan 11 2023, 16:16:36) [MSC v.1916 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> from matplotlib import pyplot as plt
>>> x = range(2, 26, 2)
>>> y = [15, 13, 14.5, 17, 20, 25, 26, 26, 27, 22, 18, 15]
>>> plt.figure(figsize=(20,8),dpi=80)
<Figure size 1600x640 with 0 Axes>
>>> plt.plot(x, y)
[<matplotlib.lines.Line2D object at 0x000001F39A878760>]
>>> plt.show()
```

![image-20230215153959093](images/image-20230215153959093.png)

## 在Linux环境中安装Python模块

在Linux环境中安装Python模块的命令与在Windows环境中的一致，同样可以使用以上三种方式进行安装。

```
pip install numpy
```



![image-20230215154406030](images/image-20230215154406030.png)

```
pip install pandas matplotlib
python -m pip install pandas matplotlib
conda install pandas matplotlib
```

## 在PyCharm中安装Python模块

除了在虚拟环境中直接使用命令安装Python模块，在PyCharm开发工具中还可以直接安装Python模块。

通过PyCharm的File -> Settings...菜单

![image-20230215155137159](images/image-20230215155137159.png)

或者右下角的Python解释器管理工具

![image-20230215155652438](images/image-20230215155652438.png)

打开PyCharm的设置界面，通过解释器环境管理中的添加按钮可以安装需要的Python模块。

![image-20230215155806196](images/image-20230215155806196.png)

在弹出的窗口中，查找自己想安装的模块，选中，点击Install Package进行安装。

![image-20230215160139267](images/image-20230215160139267.png)

安装完成后可以看到安装的pyspark模块。

![image-20230215160617539](images/image-20230215160617539.png)

# 写在后面

Python具有很高的可扩展性，在项目开发中我们会用到很多第三方的模块，用好包管理器可以大大提高我们的效率。
