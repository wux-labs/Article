# 安装Python开发工具

## 为什么需要开发工具

通常情况下，为了提高开发效率，需要使用相应的开发工具，进行Python开发也需要安装开发工具。

## Anaconda自带的开发工具

Anaconda自带有一个Python的开发工具，Spyder。

在Windows的开始菜单中可以找到Spyder的启动快捷方式。

![image-20230211230937934](images/image-20230211230937934.png)

Spyder启动的欢迎界面如图。

![image-20230211224602276](images/image-20230211224602276.png)

Spyder启动后的界面如下图。

![image-20230211230714817](images/image-20230211230714817.png)

* 菜单栏：显示可用于操纵Spyder各项功能的不同选项。
* 工具栏：通过单击图标可快速执行Spyder中最常用的操作，将鼠标悬停在某个图标上可以获取相应功能说明。
* 代码编辑器：编写Python代码的窗口，右边的行号区域显示代码所在行。
* 变量浏览器：可以方便地查看变量。
* 文件浏览器：可以方便地查看当前文件夹下的文件。
* 帮助窗口：可以快速便捷地查看帮助文档。
* 控制台：Python代码运行结果的输出地方。
* 历史浏览：按时间顺序记录输入到任何Spyder控制台的每个命令。
* 状态栏：显示当前的状态，可以看到当前的虚拟环境信息。

通过工具栏中的设置按钮，打开设置界面，在Python解释器菜单下，可以设置Python的解释器，这里我们可以选择自己创建的虚拟环境中的解释器。

![image-20230211231650539](images/image-20230211231650539.png)

在代码编辑器中编写一段代码，点击工具栏中的运行按钮，或者使用快捷键F5，可以运行当前代码。

![image-20230211231943680](images/image-20230211231943680.png)

运行结果将在控制台中输出。

![image-20230211232100378](images/image-20230211232100378.png)

我们通常不会单独开发一个Python脚本，而是需要通过项目的方式组织Python的源代码。通过Spyder的Projects菜单中的New Project菜单，可以创建新的项目。

![image-20230211232348823](images/image-20230211232348823.png)

在新建项目的对话框中输入项目的信息，点击Create按钮创建一个项目。

![image-20230211232425862](images/image-20230211232425862.png)

项目创建好后，Spyder的左侧可以看到项目树形文件结构。此时Console控制台中有一个错误提示，当前的虚拟环境中没有正确的spyder kernels，需要安装正确的kernel，并且给出了安装的命令。

![image-20230211232931213](images/image-20230211232931213.png)

打开Anaconda Prompt，按照Spyder中提示的安装命令，安装spyder kernel。

![image-20230211233144853](images/image-20230211233144853.png)

安装完成后，重新打开Spyder就不会提示错误了。新建一个Python脚本，编写代码，执行代码，可以在控制台看到正常的输出结果。

![image-20230211234108293](images/image-20230211234108293.png)

Spyder是Anaconda自带的一个强大的开发工具，为我们开发、调试脚本提供了很大的便利。

## PyCharm

PyCharm是JetBrains公司提供的强大的Python集成开发工具。PyCharm是JetBrains家族中的一个明星产品，分为两个版本，第一个版本是Professional(专业版本)，这个版本功能更加强大，主要是为Python和Web开发者而准备，是需要付费的。第二个版本是社区版，比较轻量级，主要是为Python和数据专家而准备的，是免费的。

对于有手头宽裕的，或者有其他渠道的人，可以安装PyCharm的专业版本。我们这里安装社区版本就够用了。

### 安装PyCharm

通过官方网站[PyCharm: the Python IDE for Professional Developers by JetBrains](https://www.jetbrains.com/pycharm/)下载社区版本，当前最新版本是2022.3.2。

![image-20230211235009674](images/image-20230211235009674.png)

下载完成后：

1、直接双击安装包进行安装。

2、点击Next按钮进入下一步。

![image-20230211235231637](images/image-20230211235231637.png)

3、设置好安装目录，点击Next按钮进入下一步。

![image-20230211235327905](images/image-20230211235327905.png)

4、勾选关联文件、将bin目录添加到系统变量PATH中，点击Next按钮进入下一步。

![image-20230211235408879](images/image-20230211235408879.png)

5、设置好开始菜单中的文件夹，点击Install按钮进入安装过程。

![image-20230211235531093](images/image-20230211235531093.png)

6、等待安装完成。

![image-20230211235807826](images/image-20230211235807826.png)

7、安装完成后，选择稍后重启电脑，点击Finish按钮完成安装。

![image-20230211235907591](images/image-20230211235907591.png)

### 运行PyCharm并创建项目

安装完成后就可以使用PyCharm进行项目的开发了。

通过Windows的开始菜单，点击PyCharm的快捷方式，打开PyCharm。

![image-20230212000152162](images/image-20230212000152162.png)

首先，需要同意用户协议，勾选确认同意的复选框，点击Continue按钮继续。

![image-20230212000240489](images/image-20230212000240489.png)

PyCharm启动的欢迎界面如图。

![image-20230212000355029](images/image-20230212000355029.png)

点击新建项目图标新建一个Python项目。

![image-20230212000449809](images/image-20230212000449809.png)

在项目设置界面，填写项目名称，在Python解释器栏位，选择Previously configured interpreter，选择一个预先配置的解释器环境，此时对于PyCharm来说还没有预先配置环境，所以点击Add Interpreter链接，新建一个解释器环境。

![image-20230212000642740](images/image-20230212000642740.png)

在解释器设置界面，选择Conda Environment，选择使用已存在的环境，并从虚拟环境列表中选择我们已创建好的虚拟环境，点击OK按钮完成设置。

![image-20230212000958333](images/image-20230212000958333.png)

在项目设置界面，选择刚添加的解释器，点击Create按钮创建项目。

![image-20230212001153242](images/image-20230212001153242.png)

PyCharm界面如图。

![image-20230212002159505](images/image-20230212002159505.png)

PyCharm也包含：菜单栏、工具栏、项目视图、代码编辑器、输出窗口等。

在项目中创建Python脚本，编写代码，执行代码，可以直接在输出窗口看到输出结果。

# 总结

开发工具可以大大提高我们的开发效率，帮助我们很好地管理项目代码，而且使用开发工具来对项目代码运行、调试也非常重要。我们需要选择一款好的开发工具并好好利用。
