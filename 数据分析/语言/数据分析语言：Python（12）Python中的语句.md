# Python中的语句

Python是一种高级编程语言，具有简单易学的语法，适用于各种编程任务，包括数据分析、机器学习和Web开发等。本文将详细介绍Python中的语句，包括赋值语句、条件语句、循环语句、函数语句和异常处理语句等。本文介绍Python中不同类型的语句及其用法。

## 赋值语句

赋值语句是Python中最基本的语句之一，用于将值赋给变量。在Python中，赋值语句使用等号（=）作为赋值操作符。

下面的代码将整数5赋给变量x：

```python
x = 5
print(x)
```

![image-20230308151155130](images/image-20230308151155130.png)

可以使用多个赋值语句为多个变量赋值。

下面的代码将整数5赋给变量x，将字符串"hello"赋给变量y：

```python
x = 5
y = "hello"
print(x, y)
```

![image-20230308151303122](images/image-20230308151303122.png)

在Python赋值语句中，可以同时为多个变量赋值。

下面的代码将整数5赋给变量x和y：

```python
x = y = 5
print(x, y)
```

![image-20230308151346119](images/image-20230308151346119.png)

也可以写成：

```python
x, y = 5, 5
print(x, y)
```

![image-20230308151431785](images/image-20230308151431785.png)

还可以使用赋值语句从函数返回多个值。

下面的代码定义一个函数，该函数返回两个字符串值：

```python
def get_names():
    return "Tom", "Jack"

x, y = get_names()
print(x, y)
```

在上面的代码中，调用get_names函数将返回两个字符串值，并使用赋值语句将这些值分别赋给x和y变量。

![image-20230308151528174](images/image-20230308151528174.png)

## 条件语句

条件语句是一种控制结构，用于根据条件执行不同的代码块。在Python中，条件语句使用if语句实现。if语句由关键字if、条件表达式和一个或多个代码块组成。

下面的代码使用if语句检查变量x是否大于0：

```python
x = 5

if x > 0:
    print("x大于0")
```

在上面的代码中，如果条件表达式为真（即x>0），则执行if语句后面的代码块，输出：“x大于0”。

![image-20230308151628555](images/image-20230308151628555.png)

除了if语句外，Python还支持elif和else语句。elif语句用于在多个条件之间选择，而else语句用于在所有条件都不满足时执行。

下面的代码使用if、elif和else语句来确定变量x的值：

```python
x = 0

if x > 0:
    print("x大于0")
elif x == 0:
    print("x等于0")
else:
    print("x小于0")
```

在上面的代码中，如果x大于0，则会输出：“x大于0”；如果x等于0，则会输出：“x等于0”；否则，就输出：“x小于0”。

![image-20230308151755544](images/image-20230308151755544.png)

可以在条件语句中嵌套其他条件语句。例如，下面的代码使用条件语句判断一个数字是否是偶数，并判断这个数字是否是正数、负数还是零：

```python
num = 10

if num % 2 == 0:
    if num > 0:
        print("num是大于0的偶数")
    elif num == 0:
        print("num是0")
    else:
        print("num是小于0的偶数")
else:
    if num > 0:
        print("num是大于0的奇数")
    else:
        print("num是小于0的奇数")
```

在上面的代码中，使用if语句判断变量num是否是偶数。如果是，使用嵌套的条件语句判断变量num的值是否大于0，等于0或小于0，并使用print语句输出相应的消息。如果变量num不是偶数，则使用嵌套的条件语句判断变量num的值是否大于0或小于0，并使用print语句输出相应的消息。

![image-20230308151903338](images/image-20230308151903338.png)

## 循环语句

循环语句是一种控制结构，用于重复执行代码块，直到满足特定条件为止。在Python中，有两种循环语句：for循环和while循环。

### for循环

for循环用于遍历可迭代对象，例如：列表、元组或字符串等。在Python中，for循环由关键字for、一个迭代变量和一个可迭代对象组成。例如，下面的代码使用for循环遍历列表并输出其中的每个元素：

```python
fruits = ["apple", "banana", "cherry"]

for fruit in fruits:
    print(fruit)
```

在上面的代码中，for循环遍历列表fruits，并将每个元素赋值给变量fruit。然后，使用print语句输出变量fruit的值。

![image-20230308152054042](images/image-20230308152054042.png)

可以在for循环中使用range函数来生成数字序列。例如，下面的代码使用for循环和range函数输出0到4之间的整数：

```python
for i in range(5):
    print(i)
```

在上面的代码中，for循环遍历由range函数生成的整数序列，并将每个整数赋值给变量i。然后，使用print语句输出变量i的值。

![image-20230308152155130](images/image-20230308152155130.png)

### while循环

while循环用于在条件为True时重复执行代码块。在Python中，while循环由关键字while和一个条件表达式组成。例如，下面的代码使用while循环计算1到10之间的整数的总和：

```python
i = 1
sum = 0

while i <= 10:
    sum += i
    i += 1

print("1到10的和是：", sum)
```

在上面的代码中，while循环重复执行代码块，直到变量i的值大于10。在每次循环迭代中，变量i的值递增1，并将其加到变量sum中。最后，使用print语句输出变量sum的值。

![image-20230308152303447](images/image-20230308152303447.png)

### continue语句

在Python中，continue语句用于跳过当前循环中的某个迭代，并开始下一个迭代。当程序遇到continue语句时，将跳过当前迭代中剩余的代码，并返回到循环的顶部，开始下一个迭代。

continue语句通常与if语句结合使用，用于跳过某些特定条件下的迭代。例如，下面的代码使用continue语句跳过列表中的负数，并输出列表中的正数：

```python
numbers = [1, -2, 3, -4, 5, 6, -7, 8]

print("for循环中：")
for number in numbers:
    if number < 0:
        continue
    print(number)

i = 0
print("while循环中：")
while i < len(numbers):
    i += 1
    if numbers[i-1] < 0:
        continue
    print(numbers[i-1])
```

在上面的代码中，使用for循环遍历列表numbers中的元素，并使用if语句检查每个元素是否小于0。如果元素小于0，则使用continue语句跳过当前迭代中剩余的代码，返回到循环的顶部开始下一个迭代。如果元素大于或等于0，则使用print语句输出元素的值。

在上面的例子中，列表中的负数被跳过，只输出了正数1、3、5、6、8。

![image-20230308152622404](images/image-20230308152622404.png)

> continue语句只能在循环语句中使用。如果在非循环语句中使用continue语句，程序将引发SyntaxError异常。
>
> continue语句可以在任何循环语句中使用，包括for循环、while循环。

### break语句

在Python中，break语句用于在循环中立即终止循环并跳出循环。当程序遇到break语句时，将不再执行循环中剩余的代码，并且直接跳出循环。

break语句通常与if语句结合使用，用于在某些特定条件下立即结束循环。

下面的代码使用while语句在列表中查找特定元素，找到后使用break关键字终止循环：

```python
numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
i = 0
while i < len(numbers):
    if numbers[i] == 5:
        break
    print(numbers[i])
    i += 1
```

在上面的代码中，使用while语句遍历列表中的元素。如果找到目标元素5，使用break终止循环，因此只打印出了1、2、3、4。

![image-20230308154118053](images/image-20230308154118053.png)

> break语句只能在循环语句中使用。如果在非循环语句中使用break语句，程序将引发SyntaxError异常。
>
> break语句可以在任何循环语句中使用，包括for循环、while循环等。
>
> 在使用break语句时需要小心，因为它可能会导致程序无法正常执行。如果在循环中过早地使用break语句，程序可能会错过某些重要的操作。因此，使用break语句时应该确保它不会导致意外的结果。

### continue与break的区别

continue语句用于跳过当前迭代中的语句，并开始下一个迭代。当程序遇到continue语句时，将跳过当前迭代中剩余的代码，并返回到循环的顶部，开始下一个迭代。continue通常与if语句结合使用，用于跳过某些特定条件下的迭代。

而break语句用于立即终止循环并跳出循环。当程序遇到break语句时，将不再执行循环中剩余的代码，并且直接跳出循环。break通常与if语句结合使用，用于在某些特定条件下立即结束循环。

因此，continue和break语句之间的主要区别在于它们对循环流程的影响。continue语句会跳过当前迭代中的语句并继续执行下一个迭代，而break语句会立即终止循环并跳出循环。在使用这两个关键字时，需要根据实际情况选择使用哪一个。

```python
numbers = [1, 2, 3, 4, 5]
print("使用continue：")
i = 0
while i < len(numbers):
    if numbers[i] == 3:
        i += 1
        continue
    print(numbers[i])
    i += 1

print("使用break：")
i = 0
while i < len(numbers):
    if numbers[i] == 3:
        break
    print(numbers[i])
    i += 1
```

在上面的语句循环中，continue仅跳过元素3的打印，而break则跳过了3及后续元素的打印。

![image-20230308154810054](images/image-20230308154810054.png)

## 函数语句

函数语句是一种封装代码的方式，用于将一组相关的代码块组织成可重复使用的代码块。在Python中，函数由关键字def、函数名、参数列表和一个或多个代码块组成。例如，下面的代码定义一个计算两个数之和的函数：

```python
def add(x, y):
    return x + y
```

在上面的代码中，函数名为add，参数列表为x和y，代码块使用return语句返回x和y的和。

可以使用函数调用语句来调用函数并传递参数。例如，下面的代码调用add函数并将2和3作为参数传递给它：

```python
result = add(2, 3)
print(result)
```

![image-20230308155222471](images/image-20230308155222471.png)

可以在函数定义中使用默认参数值。例如，下面的代码定义了一个函数，该函数使用默认参数值来计算两个数字的乘积：

```python
def multiply_numbers(x, y=1):
    return x * y

result1 = multiply_numbers(10)
result2 = multiply_numbers(10, 5)

print("result1是：", result1)
print("Result2是：", result2)
```

在上面的代码中，定义了一个名为multiply_numbers的函数，该函数接受两个参数x和y。其中，参数y使用了默认值1。如果调用函数时没有传递y的值，将会使用默认值1。函数返回x和y的乘积。代码中，分别调用了函数multiply_numbers两次。第一次只传递了一个参数，使用了默认值1，第二次传递了两个参数。最后，使用print语句输出两次函数调用的结果。

![image-20230308155540299](images/image-20230308155540299.png)

### pass语句

在Python中，pass语句用于在代码块中占位，表示暂时没有实现任何功能。当需要定义一个空函数或占位符函数时，可以使用pass语句作为函数体，以便稍后填充细节。

例如，下面的代码定义了一个空函数：

```pytho
def empty_function():
    pass
```

在上面的代码中，函数体中只包含一个pass语句，表示函数暂时没有实现任何功能。这种情况通常出现在编写代码框架时，需要定义函数名称和参数，但还没有实现函数的具体功能时。

除了在函数体中使用pass语句，还可以在if语句、for循环、while循环等代码块中使用。当需要暂时不执行任何操作时，可以使用pass语句占位，以便稍后填充细节。

例如，下面的代码使用pass语句定义了一个空的if语句：

```python
if x > 10:
    pass
```

在上面的代码中，if语句的条件是x > 10，但if语句体中只包含一个pass语句，表示在满足条件时不执行任何操作。这种情况通常出现在需要在代码中预留条件分支，但还没有确定具体实现方式。

## 异常处理语句

异常处理语句是一种处理异常的方式，用于在代码执行时捕获并处理可能出现的错误。在Python中，异常处理语句使用try、except、else和finally关键字组成。

例如，下面的代码使用try、except和finally语句来处理可能出现的除以零错误：

```python
try:
    result = 1 / 0
except ZeroDivisionError:
    print("除数不能是0")
finally:
    print("最终都会执行")
```

在上面的代码中，try语句包含可能出现异常的代码块，except语句用于捕获特定类型的异常ZeroDivisionError，finally语句包含无论是否出现异常都需要执行的代码块。

![image-20230308160246566](images/image-20230308160246566.png)

可以在一个异常处理语句中使用多个except语句，来捕获不同类型的异常。例如，下面的代码使用异常处理语句尝试除以0，捕获ZeroDivisionError和TypeError异常：

```python
try:
    result = 1 / "0"
except ZeroDivisionError:
    print("除数不能是0")
except TypeError:
    print("数据类型错误")
finally:
    print("最终都会执行")
```

在上面的代码中，使用try语句尝试计算1除以字符串“0”。由于这个操作是不合法的，抛出ZeroDivisionError和TypeError异常。使用两个except语句分别捕获这两种异常，并使用print语句输出相应的消息。最后，finally语句包含无论是否出现异常都需要执行的代码块。

![image-20230308160457699](images/image-20230308160457699.png)

可以在一个异常处理语句中使用else语句，在try语句未抛出异常时执行代码块。例如，下面的代码使用异常处理语句尝试打开一个文件，如果文件存在，使用else语句读取文件内容：

```python
try:
    result = 1 / 10
except ZeroDivisionError:
    print("除数不能是0")
else:
    print("结果是：", result)
finally:
    print("最终都会执行")
```

![image-20230308160752076](images/image-20230308160752076.png)

# 结论

本文介绍了Python中的各种语句，包括赋值语句、条件语句、循环语句、函数语句和异常处理语句等。这些语句是Python编程中的基本构建块，可以用于完成各种编程任务。了解这些语句的语法和用法对于Python编程非常重要。
