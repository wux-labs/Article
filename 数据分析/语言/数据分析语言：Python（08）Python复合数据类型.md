# Python复合数据类型

除了上述提到的几种基本数据类型，Python还提供了一些常用的复合数据类型，包括列表（List）、元组（Tuple）、集合（Set）和字典（Dictionary）。

## 列表（List）

列表是一种有序的数据类型，可以存储多个任意类型的数据。列表使用方括号` [ ] `来表示，每个元素之间用逗号隔开。

### 创建列表

下面是一个创建列表的例子：

```python
lst = [1, 2, 3, 'four', 'five']
print(type(lst), lst)
```

![image-20230303105244680](images/image-20230303105244680.png)

### 访问元素

列表的元素可以通过索引来访问，索引从0开始。

例如，要访问上面列表中的第一个元素，可以使用以下代码：

```python
lst = [1, 2, 3, 'four', 'five']
print(lst[0])
```

![image-20230303105343566](images/image-20230303105343566.png)

列表还支持切片操作，可以使用类似于字符串的切片语法来访问其中的元素。

例如，要访问列表中的前三个元素，可以使用以下代码：

```python
lst = [1, 2, 3, 'four', 'five']
print(lst[1:3])
```

![image-20230303105624258](images/image-20230303105624258.png)

### 内置方法

除了基本的访问和切片操作，列表还支持一系列的方法，列表有很多常用的方法，下面是一些常用的列表方法：

- append()：向列表末尾添加一个元素。
- insert()：向列表指定位置插入一个元素。
- remove()：从列表中移除指定的元素。
- pop()：从列表末尾移除一个元素。
- sort()：对列表进行排序。
- reverse()：将列表翻转。

接下来是一个例子，展示了如何使用这些方法：

```python
my_list = [1, 2, 3, 4, 5]
print("原始列表：", my_list)
# 添加一个元素
my_list.append(6)
print("添加一个元素：", my_list)
# 在指定位置插入一个元素
my_list.insert(2, "hello")
print("在指定位置插入一个元素：", my_list)
# 移除指定的元素
my_list.remove(4)
print("移除指定的元素：", my_list)
# 移除末尾的元素
my_list.pop()
print("移除末尾的元素：", my_list)
# 对列表进行排序
my_list.sort()
print("对列表进行排序：", my_list)
# 将列表翻转
my_list.reverse()
print("将列表翻转：", my_list)
```

![image-20230303105819394](images/image-20230303105819394.png)

### 列表操作

除了列表自身的一些方法外，Python还提供了一些方法可以操作列表：

* len()：求列表中元素的个数。
* +：合并两个列表。
* *：重复列表元素。
* in：判断元素是否在列表内。
* for：对列表进行遍历。

接下来是一个例子，展示了如何使用这些方法：

```python
my_list1 = [1, 2, 3]
my_list2 = ["A", "B", "C"]
print("原始列表1：", my_list1)
print("原始列表2：", my_list2)
# 求列表长度
ln = len(my_list1)
print("求列表长度：", ln)
# 合并两个列表
my_list3 = my_list1 + my_list2
print("合并两个列表：", my_list3)
# 重复列表元素
my_list4 = my_list1 * 3
print("重复列表元素：", my_list4)
# 判断元素是否在列表内
ex1 = 3 in my_list1
ex2 = 3 in my_list2
print("判断元素是否在列表内：", ex1, ex2)
# 对列表进行遍历
for x in my_list1:
    print("对列表进行遍历：", x)
```

![image-20230303110202971](images/image-20230303110202971.png)

## 元组（Tuple）

元组和列表类似，也是一种有序的数据类型，可以存储多个任意类型的数据。但是元组一旦创建，就不能再修改其中的元素。

元组使用圆括号 ( ) 来表示，每个元素之间用逗号隔开。

### 创建元组

下面是一个创建元组的例子：

```python
tuple1 = (1, 2, 3, 'four', 'five')
print(tuple1)
```

![image-20230303110655061](images/image-20230303110655061.png)

### 访问元素

元组的访问和切片操作和列表类似，也是通过索引和切片语法来实现。

```python
tuple1 = (1, 2, 3, 'four', 'five')
tuple2 = ("A", "B", "C")
print("通过索引访问：", tuple1[2])
print("通过切片访问：", tuple1[1:3])
print("元组长度：", len(tuple1))
print("元素重复：", tuple1 * 3)
print("元组合并：", tuple1 + tuple2)
print("元素判断：", "A" in tuple1, "A" in tuple2)
for x in tuple2:
    print("对元组进行遍历：", x)
```

![image-20230303111236890](images/image-20230303111236890.png)

## 集合（Set）

集合是一种无序、不重复的数据类型，用于去重或者判断一个元素是否存在。集合使用花括号 { } 来表示，每个元素之间用逗号隔开。

### 创建集合

下面是一个创建集合的例子：

```python
set1 = {1, 2, 3, 'four', 'five'}
print(set1)
```

![image-20230303114356893](images/image-20230303114356893.png)

### 基本操作

集合的操作包括添加元素、删除元素、查找、遍历、求并集、交集等。

* add(element)：向集合中添加元素
* update(set)：向集合中添加多个元素，将集合更新为和指定集合的并集
* discard(element)：移除集合中指定的元素
* remove(element)：移除集合中指定的元素，如果不存在则抛出异常
* pop()：随机移除一个元素并返回，**由于集合是无序的，因此无法确定删除的是哪个元素**。

```python
my_set = {1, 2, 3}
# 添加不存在的元素
my_set.add(4)
print("添加不存在的元素：", my_set)
# 添加已经存在的元素
my_set.add(2)
print("添加已经存在的元素：", my_set)
# 向集合中添加多个元素
my_set.update([5, 6])
my_set.update((7, 8))
my_set.update({9, 10})
print("向集合中添加多个元素：", my_set)
# 移除集合中指定的元素
my_set.remove(3)
my_set.discard(5)
pp = my_set.pop()
print("移除集合中指定的元素：", my_set, pp)
for x in my_set:
    print("遍历集合：", x)
```

![image-20230303115709701](images/image-20230303115709701.png)

### 其他操作

可以使用一些内置的方法来对集合进行操作。以下是一些常用的集合方法：

- clear()：清空集合
- copy()：复制集合
- difference(set)：返回集合和指定集合的差集
- difference_update(set)：移除集合中和指定集合相同的元素
- intersection(set)：返回集合和指定集合的交集
- intersection_update(set)：保留集合中和指定集合相同的元素
- isdisjoint(set)：判断两个集合是否没有共同元素
- issubset(set)：判断一个集合是否是另一个集合的子集
- issuperset(set)：判断一个集合是否是另一个集合的超集
- symmetric_difference(set)：返回集合和指定集合的对称差集
- symmetric_difference_update(set)：将集合更新为和指定集合的对称差集
- union(set)：返回集合和指定集合的并集

下面是一些示例代码：

```python
# 创建集合
fruits = {"apple", "banana", "cherry"}
# 添加元素
fruits.add("orange")
print(fruits)  # 输出: {'banana', 'apple', 'orange', 'cherry'}
# 移除元素
fruits.remove("banana")
print(fruits)  # 输出: {'apple', 'orange', 'cherry'}
# 清空集合
fruits.clear()
print(fruits)  # 输出: set()
# 复制集合
fruits = {"apple", "banana", "cherry"}
fruits_copy = fruits.copy()
print(fruits_copy)  # 输出: {'banana', 'apple', 'cherry'}
# 求交集
x = {"apple", "banana", "cherry"}
y = {"google", "microsoft", "apple"}
z = x.intersection(y)
print(z)  # 输出: {'apple'}
# 求并集
x = {"apple", "banana", "cherry"}
y = {"google", "microsoft", "apple"}
z = x.union(y)
print(z)  # 输出: {'banana', 'apple', 'google', 'cherry', 'microsoft'}
# 判断是否是子集
x = {"a", "b", "c"}
y = {"f", "e", "d", "c", "b", "a"}
z = x.issubset(y)
print(z)  # 输出: True
```

## 字典（Dictionary）

字典是一种无序的键值对（key-value）数据类型，可以用来存储任意类型的数据。字典使用花括号 { } 来表示，每个键值对之间用冒号 : 隔开，键值对之间用逗号隔开。

### 创建字典

可以使用花括号 {} 或者 dict() 函数来创建字典。使用花括号创建字典时，键-值对之间用冒号 : 分隔，每个键-值对之间用逗号 , 分隔。

```python
# 使用花括号创建字典
my_dict1 = {'apple': 3, 'banana': 5, 'orange': 2}
print(my_dict1)
# 使用 dict() 函数创建字典
my_dict2 = dict(apple=3, banana=5, orange=2)
print(my_dict2)
```

![image-20230303120531172](images/image-20230303120531172.png)

### 访问元素

可以使用键来访问字典中的元素。如果键不存在，则会抛出 KeyError 异常。

```python
my_dict = {'apple': 3, 'banana': 5, 'orange': 2}

print(my_dict['apple'])  # 输出 3
print(my_dict['pear'])   # 抛出 KeyError 异常
```

![image-20230303120745655](images/image-20230303120745655.png)

可以使用 get() 方法来访问字典中的元素。如果键不存在，则会返回 None 或者指定的默认值。

```python
my_dict = {'apple': 3, 'banana': 5, 'orange': 2}

print(my_dict.get('apple'))      # 输出 3
print(my_dict.get('pear'))       # 输出 None
print(my_dict.get('pear', 0))    # 输出 0
```

![image-20230303120949098](images/image-20230303120949098.png)

### 基本操作

Python字典包含了以下内置方法：

* clear()：删除字典内所有元素
* copy()：返回一个字典的浅复制
* get(key, default=None)：返回指定键的值，如果值不在字典中返回default值。
* has_key(key)：如果键在字典dict里返回true，否则返回false。
* items()：以列表返回可遍历的(键, 值) 元组数组。
* keys()：以列表返回一个字典所有的键。
* values()：以列表返回字典中的所有值。

# 写在后面

在本篇文章中，我们介绍了Python的几种复合数据类型：列表、元组、集合、字典，同时，我们还讨论了它们的一些操作。
